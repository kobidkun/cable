<?php

namespace App\Model\Car;

//use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use SMartins\PassportMultiauth\HasMultiAuthApiTokens;

class CarAdmin extends Authenticatable
{
    use Notifiable, HasMultiAuthApiTokens;
    protected $guard = 'caradmin';
    public $incrementing = false;
    public function getAuthPassword () {

        return $this->car_admin_password;

    }
    protected $fillable = [
        'id', 'car_admin_name', 'car_admin_email', 'car_admin_phone', 'car_admin_password'
    ];
    public function cars(){
        return $this->hasMany('App\Model\Car\Car','car_admin_uuid','id');
    }

}
