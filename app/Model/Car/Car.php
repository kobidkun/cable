<?php

namespace App\Model\Car;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Car extends Model
{
    use Notifiable;

    protected $fillable = [
        'car_number',
        'car_admin_uuid',
        'car_type',
        'car_model',
        'car_fuel_type',
        'driver_uuid',
        'car_capacity',
        'car_image',
        'car_city',
        'car_lat',
        'car_long',
        'car_altitude',
        'is_moving'
    ];

    public function car_admins123(){
        return $this->hasOne('App\Model\Car\CarAdmin','id','car_admin_uuid');
    }
    public function drivers(){
        return $this->hasOne('App\Model\Car\Driver','id','driver_uuid');
    }
    public function trip_drivers(){
        return $this->hasMany('App\Model\Trip\TripDriver','car_id','id');
    }
    public function trip_solos(){
        return $this->hasMany('App\Model\Trip\TripSolo','car_id','id');
    }

    public function create_cities(){
        return $this->hasOne('App\Model\City\CreateCity','id','car_city');
    }
    public function car_type123(){
        return $this->hasOne('App\Model\Car\CarType','id','car_type');
    }
}
