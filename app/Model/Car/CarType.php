<?php

namespace App\Model\Car;

use Illuminate\Database\Eloquent\Model;

class CarType extends Model
{
    protected $fillable = [
        'car_type_name',
        'fare_per_hr',
        'fare_per_km'
    ];
    public function cars(){
        $this->hasMany('App\Model\Car\Car', 'car_type','id');
    }
}
