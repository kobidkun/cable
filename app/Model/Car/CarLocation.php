<?php

namespace App\Model\Car;

use Illuminate\Database\Eloquent\Model;

class CarLocation extends Model
{
    protected $primaryKey = 'car_location_id';

    public $incrementing = false;

    protected $fillable =[
        'trip_driver_id',
        'car_lat',
        'car_long',
        'car_altitude',
        'is_moving'
    ];

    public function trip_drivers(){
        return $this->hasOne('App\Model\Trip\TripDriver','id','trip_driver_id');
    }
}
