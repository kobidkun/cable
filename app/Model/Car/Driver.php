<?php

namespace App\Model\Car;

//use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Driver extends Authenticatable
{
    use Notifiable;
    protected $guard = 'driver';
    public $incrementing = false;
    public function getAuthPassword () {

        return $this->driver_password;

    }
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'driver_name', 'driver_email', 'driver_password', 'driver_phone', 'driver_license', 'car_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'driver_password', 'remember_token',
    ];
    public function cars123(){
        return $this->hasOne('App\Model\Car\Car','id','car_id');
    }
}
