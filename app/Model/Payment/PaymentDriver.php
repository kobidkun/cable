<?php

namespace App\Model\Payment;

use Illuminate\Database\Eloquent\Model;

class PaymentDriver extends Model
{
    public function trip_drivers(){
        return $this->hasOne('App\Model\Trip\TripDriver','id','trip_driver_id');
    }
}
