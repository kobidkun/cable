<?php

namespace App\Model\Payment;

use Illuminate\Database\Eloquent\Model;

class PaymentSolo extends Model
{
    public function trip_solos(){
        return $this->hasOne('App\Model\Trip\TripSolo','id','trip_solo_id');
    }
}
