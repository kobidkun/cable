<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Search extends Model
{
    use Notifiable;

    protected $fillable = [
        'customer_id',
        'origin_lat',
        'origin_long',
        'destination_lat',
        'destination_long'
    ];
    public function customers(){
        return $this->hasOne('App\Model\Customer\Customer', 'id','customer_id');
    }
}
