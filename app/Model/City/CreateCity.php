<?php

namespace App\Model\City;

use Illuminate\Database\Eloquent\Model;

class CreateCity extends Model
{
    protected $fillable = [
        'name',
        'lat',
        'long'

    ];
    public function cars123(){
        return $this->hasMany('App\Model\Car\Car','car_city','id');
    }
}
