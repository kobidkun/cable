<?php

namespace App\Model\Trip;

use Illuminate\Database\Eloquent\Model;

class TripRatingDriver extends Model
{
    public function trip_drivers(){
        return $this->hasOne('App\Model\Trip\TripDriver', 'id','trip_driver_id');
    }
}
