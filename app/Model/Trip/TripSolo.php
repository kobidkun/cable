<?php

namespace App\Model\Trip;

use Illuminate\Database\Eloquent\Model;

class TripSolo extends Model
{
    public function cars(){
        return $this->hasOne('App\Model\Car\Car','id','car_id');
    }
    public function trip_rating_solos(){
        return $this->hasOne('App\Model\Trip\TripRatingSolo', 'id','trip_rating_solo_id');
    }
    public function customers(){
        return $this->hasOne('App\Model\Customer\Customer','id','customer_uuid');
    }
    public function payment_solos(){
        return $this->hasOne('App\Model\Payment\PaymentSolo','payment_solo_id','payment_solo_id');
    }
}
