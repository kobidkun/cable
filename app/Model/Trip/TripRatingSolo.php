<?php

namespace App\Model\Trip;

use Illuminate\Database\Eloquent\Model;

class TripRatingSolo extends Model
{

    public function trip_solos(){
        return $this->hasOne('App\Model\Trip\TripSolo', 'id','trip_solo_id');
    }
}
