<?php

namespace App\Model\Trip;

use Illuminate\Database\Eloquent\Model;

class TripDriver extends Model{

    protected $fillable = [
        'customer_uuid',
        'car_id',
        'driver_uuid',
        'pickup_lat',
        'pickup_long',
        'pickup_address',
        'drop_lat',
        'drop_long',
        'total_distance',
        'drop_address'
    ];

    public function cars(){
        return $this->hasOne('App\Model\Car\Car','id','car_id');
    }

    public function customers(){
        return $this->hasOne('App\Model\Customer\Customer','id','customer_uuid');
    }

    public function trip_rating_drivers(){
        return $this->hasOne('App\Model\Trip\TripRatingDriver', 'trip_driver_id','id');
    }

    public function drivers(){
        return $this->hasOne('App\Model\Car\Driver','id','driver_uuid');
    }

    public function payment_drivers(){
        return $this->hasOne('App\Model\Payment\PaymentDriver','payment_driver_uuid','payment_driver_uuid');
    }
    public function car_locations(){
        return $this->hasMany('App\Model\Car\CarLocation','trip_driver_id', 'id');
    }
}
