<?php

namespace App\Model\Customer;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use SMartins\PassportMultiauth\HasMultiAuthApiTokens;

class Customer extends Authenticatable
{
    use HasMultiAuthApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'customer_password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    public function trip_drivers(){
        return $this->hasMany('App\Model\Trip\TripDriver','customer_uuid','id');
    }
    public function trip_solos(){
        return $this->hasMany('App\Model\Trip\TripSolo', 'customer_uuid', 'id');
    }
    public function searches(){
        return $this->hasMany('App\Model\Search', 'customer_id', 'id');
    }
}
