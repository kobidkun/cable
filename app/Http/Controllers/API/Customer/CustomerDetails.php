<?php

namespace App\Http\Controllers\API\Customer;

use App\Model\Customer\Customer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CustomerDetails extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:customer-api');
    }

    public function ProfileCustomer(Request $request){

        $user = $request->user();


        return response()->json($user,200);

    }

    public function OTP(Request $request){



        $user = $request->user();

        //Your authentication key
        $authKey = "143565AIE7jf1Mb5abd98a0";
        $mobileNumber = $user->phone;
        //Multiple mobiles numbers separated by comma
//        $mobileNumber = "8172071965";

        //Sender ID,While using route4 sender id should be 6 characters long.
        $senderId = "TECION";

        $name = $user->name;

        $otp = rand(1000, 9999);
        //Your message to send, Add URL encoding here.
        $message = urlencode("Hello ".$name.", Your OTP is ".$otp);

        //Define route
        $route = "4";

        //Prepare you post parameters
        $postData = array(
            'authkey' => $authKey,
            'mobiles' => $mobileNumber,
            'message' => $message,
            'sender' => $senderId,
            'route' => $route
        );

        //API URL
        $url="http://api.msg91.com/api/sendhttp.php";

        // init the resource
        $ch = curl_init();
        curl_setopt_array($ch, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $postData
            //,CURLOPT_FOLLOWLOCATION => true
        ));


//Ignore SSL certificate verification
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);


//get response
        $output = curl_exec($ch);

//Print error if any
        if(curl_errno($ch))
        {
            echo 'error:' . curl_error($ch);
        }

        curl_close($ch);


        $cust = Customer::find($user->id);

        $cust->otp_verifier = $otp;

        $cust->save();


        return response()->json('success', 200);





    }

    public function otpVerify(Request $request){

        $user = $request->user();
        $cust = Customer::find($user->id);

        if ($cust->otp_verifier === $request->otp){
            return response()->json(true,200);
        } else {
            return response()->json(false,401);
        }




    }

}
