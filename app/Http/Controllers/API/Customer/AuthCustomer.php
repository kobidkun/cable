<?php

namespace App\Http\Controllers\API\Customer;

use App\Http\Requests\API\Customer\Register;
use App\Model\Customer\Customer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AuthCustomer extends Controller
{
    public function RegisterCustomer(Register $request){

        $a = new Customer();

        $a->customer_lat = $request->customer_lat;
        $a->customer_long = $request->customer_long;
        $a->customer_altitude = $request->customer_altitude;
        $a->email_verified_at = $request->email_verified_at;
        $a->phone_verified_at = $request->phone_verified_at;
        $a->verifier = $request->verifier;
        $a->otp_verifier = $request->otp_verifier;
        $a->name = $request->name;
        $a->customer_city = $request->customer_city;
        $a->phone = $request->phone;
        $a->email = $request->email;

        $a->customer_license = $request->customer_license;
        $a->password = bcrypt($request->password);
        $a->save();

        return response()->json('success',200);



    }
}
