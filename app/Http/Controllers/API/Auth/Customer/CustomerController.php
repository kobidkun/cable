<?php
namespace App\Http\Controllers\API\Auth\Customer;
use Carbon\Carbon;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use App;
use App\Model\Customer\Customer;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class CustomerController extends Controller
{
    use ValidatesRequests;
    public $successStatus = 200;
    public function __construct()
    {
        $this->middleware('auth:customer',['only' => 'index','edit']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('auth.customer.dashboard');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('auth.customer.auth.register');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // validate the data
       /* $this->validate($request, [
            'customer_uuid'          => 'required',
            'customer_name'          => 'required',
            'customer_email'         => 'required',
            'customer_password'      => 'required'
        ]);
        // store in the database
        $customers = new Customer;
        $customers->id = $request->customer_uuid;
        $customers->customer_name = $request->customer_name;
        $customers->customer_email = $request->customer_email;
        $customers->customer_phone = $request->customer_phone;
        $customers->customer_license = $request->customer_license;
        $customers->customer_city = $request->customer_city;
        $customers->customer_lat = $request->customer_lat;
        $customers->customer_long = $request->customer_long;
        $customers->customer_altitude = $request->customer_altitude;
        $customers->customer_password=bcrypt($request->customer_password);
        $customers->save();
        return redirect()->route('customer.auth.login');*/
        //
        $validator = Validator::make($request->all(), [
            'customer_uuid'          => 'required',
//            'customer_name'          => 'required',
//            'customer_email'         => 'required',
//            'customer_password'      => 'required',
            'customer_phone'        => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:10',
        ]);
        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);
        }
        $this->sendSMS($request);

        /*$input = $request->all();
        $input['password'] = bcrypt($input['password']);
        $user = Customer::create($input);
        $success['token'] =  $user->createToken('MyApp')-> accessToken;
        $success['name'] =  $user->name;
        return response()->json(['success'=>$success], $this-> successStatus);*/

    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function verifier($verifier, $email){
        $customerdata = Customer::where('customer_email', decrypt($email))->first();
        if ($verifier == $customerdata->verifier){
            $customerdata->email_verified_at = Carbon::now();
            $customerdata->save();
            return redirect()->route('customer.dashboard');
        }
        else{
            return redirect()->route('customer.dashboard');
        }
    }
    public function sendSMS(Request $request){
        //Your authentication key
        $authKey = "143565AIE7jf1Mb5abd98a0";
        $mobileNumber = $request->customer_phone;
        //Multiple mobiles numbers separated by comma
//        $mobileNumber = "8172071965";

        //Sender ID,While using route4 sender id should be 6 characters long.
        $senderId = "TECION";
        $receiverId = $request->customer_uuid;

        /*$name = $request->admin_name;*/

        $otp = rand(1000, 9999);
        //Your message to send, Add URL encoding here.
        $message = urlencode("Hello, Your OTP is ".$otp);

        //Define route
        $route = "4";

        //Prepare you post parameters
        $postData = array(
            'authkey' => $authKey,
            'mobiles' => $mobileNumber,
            'message' => $message,
            'sender'  => $senderId,
            'route'   => $route
        );

        //API URL
        $url="http://api.msg91.com/api/sendhttp.php";

        // init the resource
        $ch = curl_init();
        curl_setopt_array($ch, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $postData
            //,CURLOPT_FOLLOWLOCATION => true
        ));


//Ignore SSL certificate verification
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);


//get response
        $output = curl_exec($ch);

//Print error if any
        if(curl_errno($ch))
        {
            echo 'error:' . curl_error($ch);
        }

        curl_close($ch);
//        echo $output;
        /*$encryptedOTP = encrypt($otp);
        $admin = Customer::where('admin_phone', $mobileNumber)->first();
        $admin->otp_verifier = $encryptedOTP;
        $admin->save();*/
        $customer = new Customer();
        $customer->otp_verifier = encrypt($otp);
        

    }
    public function otpform(){
        return view('auth.admin.auth.otpverify');
    }
    public function otp(Request $request){

        $receiverNo = $request->admin_phone;
        $admin = Customer::where('admin_phone', $receiverNo)->first();
        $decryptedOTP = decrypt($admin->otp_verifier);
        $otpEntered = $request->otpEntered;
        if($otpEntered == $decryptedOTP){
            $admin->phone_verified_at = Carbon::now();
            $admin->save();
            return redirect()->route('admin.dashboard');
        }
        else{
            return redirect()->route('admin.dashboard');
        }
    }
}
