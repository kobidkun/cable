<?php
namespace App\Http\Controllers\API\Auth\CarAdmin;
//use App\Model\Master\Admin;
use App\Model\Master\Admin;
use Carbon\Carbon;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use App;
use App\Model\Car\CarAdmin;
use App\Http\Controllers\Controller;
class CarAdminController extends Controller
{
    use ValidatesRequests;
    public function __construct()
    {
        $this->middleware('auth:caradmin',['only' => 'index','edit']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('auth.caradmin.dashboard');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('auth.caradmin.auth.register');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // validate the data
        $this->validate($request, [
            'car_admin_uuid'          => 'required',
            'car_admin_name'          => 'required',
            'car_admin_phone'         => 'required',
            'car_admin_email'         => 'required',
            'car_admin_password'      => 'required'
        ]);
        // store in the database
        $caradmins = new CarAdmin();
        $caradmins->id = $request->car_admin_uuid;
        $caradmins->car_admin_name = $request->car_admin_name;
        $caradmins->car_admin_email = $request->car_admin_email;
        $caradmins->car_admin_phone = $request->car_admin_phone;
        $caradmins->car_admin_password=bcrypt($request->car_admin_password);
        $caradmins->save();
        return redirect()->route('caradmin.auth.login');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function verifier($verifier, $email){
        $caradmindata = CarAdmin::where('car_admin_email', decrypt($email))->first();
        if ($verifier == $caradmindata->verifier){
            $caradmindata->email_verified_at = Carbon::now();
            $caradmindata->save();
            return redirect()->route('caradmin.dashboard');
        }
        else{
            return redirect()->route('caradmin.dashboard');
        }
    }
    public function sendSMS(Request $request){
        //Your authentication key
        $authKey = "143565AIE7jf1Mb5abd98a0";
        $mobileNumber = $request->car_admin_phone;
        //Multiple mobiles numbers separated by comma
//        $mobileNumber = "8172071965";

        //Sender ID,While using route4 sender id should be 6 characters long.
        $senderId = "TECION";
        $receiverId = $request->car_admin_uuid;

        $name = $request->car_admin_name;

        $otp = rand(1000, 9999);
        //Your message to send, Add URL encoding here.
        $message = urlencode("Hello ".$name.", Your OTP is ".$otp);

        //Define route
        $route = "4";

        //Prepare you post parameters
        $postData = array(
            'authkey' => $authKey,
            'mobiles' => $mobileNumber,
            'message' => $message,
            'sender'  => $senderId,
            'route'   => $route
        );

        //API URL
        $url="http://api.msg91.com/api/sendhttp.php";

        // init the resource
        $ch = curl_init();
        curl_setopt_array($ch, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $postData
            //,CURLOPT_FOLLOWLOCATION => true
        ));


//Ignore SSL certificate verification
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);


//get response
        $output = curl_exec($ch);

//Print error if any
        if(curl_errno($ch))
        {
            echo 'error:' . curl_error($ch);
        }

        curl_close($ch);
//        echo $output;
        $encryptedOTP = encrypt($otp);
        $caradmin = CarAdmin::where('car_admin_phone', $mobileNumber)->first();
        $caradmin->otp_verifier = $encryptedOTP;
        $caradmin->save();
        return view('auth.caradmin.auth.otpverify');
    }
    public function otpform(){
        return view('auth.caradmin.auth.otpverify');
    }
    public function otp(Request $request){

        $receiverNo = $request->car_admin_phone;
        $caradmin = CarAdmin::where('car_admin_phone', $receiverNo)->first();
        $decryptedOTP = decrypt($caradmin->otp_verifier);
        $otpEntered = $request->otpEntered;
        if($otpEntered == $decryptedOTP){
            $caradmin->phone_verified_at = Carbon::now();
            $caradmin->save();
            return redirect()->route('caradmin.dashboard');
        }
        else{
            return redirect()->route('caradmin.dashboard');
        }
    }

}
