<?php

namespace App\Http\Controllers\API\Auth\CarAdmin;

use App\Http\Controllers\Controller;
use App\Mail\CarAdminVerifierMail;
use App\Model\Car\CarAdmin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class MailController extends Controller
{
    protected $verifier = '';
    public function authentication(Request $request){
        $caradmin = CarAdmin::where('car_admin_uuid', $request->car_admin_uuid)->first();
        $verifier = substr(str_shuffle('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'),0, 50);
        $caradmin->verifier = $verifier;
        $caradmin->save();
        $to = $request->car_admin_email;
        $email = encrypt($to);
        $name = $request->car_admin_name;
        Mail::to($to)->send(new CarAdminVerifierMail($verifier, $name, $email));
        return redirect()->route('caradmin.emailresend');
    }
    public function emailresend(){
        return view('mail.resendemail');
    }
}
