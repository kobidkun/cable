<?php
namespace App\Http\Controllers\API\Auth\Driver;
use Carbon\Carbon;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use App;
use App\Model\Car\Driver;
use App\Http\Controllers\Controller;
class DriverController extends Controller
{
    use App\Traits\UploadTrait;
    use ValidatesRequests;
    public function __construct()
    {
        $this->middleware('auth:driver',['only' => 'index','edit']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('auth.driver.dashboard');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('auth.driver.auth.register');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // validate the data
        $this->validate($request, [
            'driver_uuid'          => 'required',
            'driver_name'          => 'required',
            'driver_phone'         => 'required',
            'driver_email'         => 'required',
            'driver_password'      => 'required',
            'driver_license'       => 'required',
            'car_id'             => 'required'
        ]);
        // store in the database
        $drivers = new Driver;
        $drivers->id = $request->driver_uuid;
        $drivers->driver_name = $request->driver_name;
        $drivers->driver_email = $request->driver_email;
        $drivers->driver_phone = $request->driver_phone;
        $drivers->driver_license = $request->driver_license;
        $drivers->car_id = $request->car_id;
        $drivers->driver_password=bcrypt($request->driver_password);

        // Check if a profile image has been uploaded
        if ($request->has('driver_image')) {
            // Get image file
            $image = $request->file('driver_image');
            // Make a image name based on user name and current timestamp
//            $name = str_slug($request->input('name')).'_'.time();
            $name = str_slug($request->input('driver_name'));
            // Define folder path
            $folder = '/uploads/images/drivers/';
            // Make a file path where image will be stored [ folder path + file name + file extension]
            $filePath = $folder . $name. '.' . $image->getClientOriginalExtension();
            // Upload image
            $this->uploadOne($image, $folder, 'public', $name);
            // Set user profile image path in database to filePath
            $drivers->driver_image = $filePath;
        }

        $drivers->save();
        return redirect()->route('driver.auth.login');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function verifier($verifier, $email){
        $driverdata = Driver::where('driver_email', decrypt($email))->first();
        if ($verifier == $driverdata->verifier){
            $driverdata->email_verified_at = Carbon::now();
            $driverdata->save();
            return redirect()->route('driver.dashboard');
        }
        else{
            return redirect()->route('driver.dashboard');
        }
    }
}
