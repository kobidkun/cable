<?php

namespace App\Http\Controllers\API\Auth\Driver;

use App\Mail\VerificationMail;
use App\Model\Car\Driver;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;

class MailController extends Controller
{
    protected $verifier = '';
    public function authentication(Request $request){
        $driver = Driver::where('driver_uuid', $request->driver_uuid)->first();
        $verifier = substr(str_shuffle('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'),0, 50);
        $driver->verifier = $verifier;
        $driver->save();
        $to = $request->driver_email;
        $email = encrypt($to);
        $name = $request->driver_name;
        Mail::to($to)->send(new VerificationMail($verifier, $name, $email));
        return redirect()->route('driver.emailresend');
    }
    public function emailresend(){
        return view('mail.resendemail');
    }
}
