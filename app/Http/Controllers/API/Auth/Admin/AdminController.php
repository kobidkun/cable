<?php
namespace App\Http\Controllers\API\Auth\Admin;
use App\User;
use Carbon\Carbon;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use App;
use App\Model\Master\Admin;
use App\Http\Controllers\Controller;
use App\Model\Car\Car;
class AdminController extends Controller
{
    use ValidatesRequests;
    public function __construct()
    {
        $this->middleware('auth:admin',['only' => 'index','edit']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('auth.admin.dashboard');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('auth.admin.auth.register');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // validate the data
        $this->validate($request, [
            'admin_name'          => 'required',
            'admin_email'         => 'required',
            'admin_password'      => 'required'
        ]);
        // store in the database
        $admins = new Admin();
        $admins->admin_name = $request->admin_name;
        $admins->admin_email = $request->admin_email;
        $admins->admin_phone = '+91'.$request->admin_phone;
        $admins->password=bcrypt($request->admin_password);
        $admins->save();
        return response()->json('');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function verifier($verifier, $email){
        $admindata = Admin::where('admin_email', decrypt($email))->first();
        if ($verifier == $admindata->verifier){
            $admindata->email_verified_at = Carbon::now();
            $admindata->save();
            return redirect()->route('admin.dashboard');
        }
        else{
            return redirect()->route('admin.dashboard');
        }
    }
    public function sendSMS(Request $request){
        //Your authentication key
        $authKey = "143565AIE7jf1Mb5abd98a0";
        $mobileNumber = $request->admin_phone;
        //Multiple mobiles numbers separated by comma
//        $mobileNumber = "8172071965";

        //Sender ID,While using route4 sender id should be 6 characters long.
        $senderId = "TECION";
        $receiverId = $request->admin_id;

        $name = $request->admin_name;

        $otp = rand(1000, 9999);
        //Your message to send, Add URL encoding here.
        $message = urlencode("Hello ".$name.", Your OTP is ".$otp);

        //Define route
        $route = "4";

        //Prepare you post parameters
        $postData = array(
            'authkey' => $authKey,
            'mobiles' => $mobileNumber,
            'message' => $message,
            'sender'  => $senderId,
            'route'   => $route
        );

        //API URL
        $url="http://api.msg91.com/api/sendhttp.php";

        // init the resource
        $ch = curl_init();
        curl_setopt_array($ch, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $postData
            //,CURLOPT_FOLLOWLOCATION => true
        ));


//Ignore SSL certificate verification
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);


//get response
        $output = curl_exec($ch);

//Print error if any
        if(curl_errno($ch))
        {
            echo 'error:' . curl_error($ch);
        }

        curl_close($ch);
//        echo $output;
        $encryptedOTP = encrypt($otp);
        $admin = Admin::where('admin_phone', $mobileNumber)->first();
        $admin->otp_verifier = $encryptedOTP;
        $admin->save();
        return view('auth.admin.auth.otpverify');
    }
    public function otpform(){
        return view('auth.admin.auth.otpverify');
    }
    public function otp(Request $request){

        $receiverNo = $request->admin_phone;
        $admin = Admin::where('admin_phone', $receiverNo)->first();
        $decryptedOTP = decrypt($admin->otp_verifier);
        $otpEntered = $request->otpEntered;
        if($otpEntered == $decryptedOTP){
            $admin->phone_verified_at = Carbon::now();
            $admin->save();
            return redirect()->route('admin.dashboard');
        }
        else{
            return redirect()->route('admin.dashboard');
        }
    }

}
