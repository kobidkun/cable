<?php

namespace App\Http\Controllers\Auth\Admin;

use App\Http\Controllers\Controller;
use App\Mail\VerificationMail;
use App\Model\Master\Admin;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class MailController extends Controller
{
    protected $verifier = '';
    public function authentication(Request $request){
        $admin = Admin::where('admin_id', $request->admin_id)->first();
        $verifier = substr(str_shuffle('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'),0, 50);
        $admin->verifier = $verifier;
        $admin->save();
        $to = $request->admin_email;
        $email = encrypt($to);
        $name = $request->admin_name;
        Mail::to($to)->send(new VerificationMail($verifier, $name, $email));
        return redirect()->route('admin.emailresend');
    }
    public function emailresend(){
        return view('mail.resendemail');
    }
}
