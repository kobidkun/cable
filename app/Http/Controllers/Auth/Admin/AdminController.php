<?php
namespace App\Http\Controllers\Auth\Admin;
use App\Model\Car\Car;
use App\User;
use Carbon\Carbon;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use App;
use App\Model\Master\Admin;
use App\Http\Controllers\Controller;
use phpseclib\Crypt\TripleDES;

class AdminController extends Controller
{
    use ValidatesRequests;
    public function __construct()
    {
        $this->middleware('auth:admin',['except' => 'store', 'create']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('auth.admin.dashboard');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('auth.admin.auth.register');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // validate the data
        $this->validate($request, [
            'admin_name'          => 'required',
            'admin_email'         => 'required',
            'admin_password'      => 'required'
        ]);
        // store in the database
        $admins = new Admin();
        $admins->admin_name = $request->admin_name;
        $admins->admin_email = $request->admin_email;
        $admins->admin_phone = '+91'.$request->admin_phone;
        $admins->password=bcrypt($request->admin_password);
        $admins->save();
        return redirect()->route('admin.auth.login');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function verifier($verifier, $email){
        $admindata = Admin::where('admin_email', decrypt($email))->first();
        if ($verifier == $admindata->verifier){
            $admindata->email_verified_at = Carbon::now();
            $admindata->save();
            return redirect()->route('admin.dashboard');
        }
        else{
            return redirect()->route('admin.dashboard');
        }
    }
    public function sendSMS(Request $request){
        //Your authentication key
        $authKey = "143565AIE7jf1Mb5abd98a0";
        $mobileNumber = $request->admin_phone;
        //Multiple mobiles numbers separated by comma
//        $mobileNumber = "8172071965";

        //Sender ID,While using route4 sender id should be 6 characters long.
        $senderId = "TECION";
        $receiverId = $request->admin_id;

        $name = $request->admin_name;

        $otp = rand(1000, 9999);
        //Your message to send, Add URL encoding here.
        $message = urlencode("Hello ".$name.", Your OTP is ".$otp);

        //Define route
        $route = "4";

        //Prepare you post parameters
        $postData = array(
            'authkey' => $authKey,
            'mobiles' => $mobileNumber,
            'message' => $message,
            'sender'  => $senderId,
            'route'   => $route
        );

        //API URL
        $url="http://api.msg91.com/api/sendhttp.php";

        // init the resource
        $ch = curl_init();
        curl_setopt_array($ch, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $postData
            //,CURLOPT_FOLLOWLOCATION => true
        ));


//Ignore SSL certificate verification
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);


//get response
        $output = curl_exec($ch);

//Print error if any
        if(curl_errno($ch))
        {
            echo 'error:' . curl_error($ch);
        }

        curl_close($ch);
//        echo $output;
        $encryptedOTP = encrypt($otp);
        $admin = Admin::where('admin_phone', $mobileNumber)->first();
        $admin->otp_verifier = $encryptedOTP;
        $admin->save();
        return view('auth.admin.auth.otpverify');
    }
    public function otpform(){
        return view('auth.admin.auth.otpverify');
    }
    public function otp(Request $request){

        $receiverNo = $request->admin_phone;
        $admin = Admin::where('admin_phone', $receiverNo)->first();
        $decryptedOTP = decrypt($admin->otp_verifier);
        $otpEntered = $request->otpEntered;
        if($otpEntered == $decryptedOTP){
            $admin->phone_verified_at = Carbon::now();
            $admin->save();
            return redirect()->route('admin.dashboard');
        }
        else{
            return redirect()->route('admin.dashboard');
        }
    }

    public function addCarPage(){
        $caradmins = App\Model\Car\CarAdmin::all();
        $drivers = App\Model\Car\Driver::all();
        $cities = App\Model\City\CreateCity::all();
        $cartypes = App\Model\Car\CarType::all();
        return view('AdminPanel.pages.addCar')->with([
           'caradmins' => $caradmins,
           'drivers' => $drivers,
           'cities' => $cities,
           'cartypes' => $cartypes
        ]);
    }
    public function addCarAdminPage(){
        return view('AdminPanel.pages.addCarAdmin');
    }
    public function addCityPage(){
        return view('AdminPanel.pages.addCity');
    }
    public function addCarTypePage(){
        return view('AdminPanel.pages.addcartype');
    }
    public function addDriverPage(){
        $allcars = Car::all();
        return view('AdminPanel.pages.addDriver')->with([
            'allcars' => $allcars
        ]);
    }
    public function addTripDriverPage(){
        return view('AdminPanel.pages.addTripDrivers');
    }
    public function addCustomerPage(){
        return view('AdminPanel.pages.addCustomer');
    }
    /*public function add(Request $request){
    App\Model\Customer\Customer::create($request->all());
    return back();
    }

    public function addCustomer(Request $request){
        App\Model\Customer\Customer::create($request->all());
        return back();
    }
    public function addCustomer(Request $request){
        App\Model\Customer\Customer::create($request->all());
        return back();
    }
    public function addCustomer(Request $request){
        App\Model\Customer\Customer::create($request->all());
        return back();
    }
    public function addCustomer(Request $request){
        App\Model\Customer\Customer::create($request->all());
        return back();
    }*/

    public function addcar(Request $request)
    {
        Car::create($request->all());

        return back();
    }
    public function addCustomer(Request $request){
        App\Model\Customer\Customer::create($request->all());
        return back();
    }
    public function addDriver(Request $request){
        App\Model\Car\Driver::create($request->all());
        return back();
    }
    public function addCarAdmin(Request $request){
        $input = $request->all();
        $input['car_admin_password'] = bcrypt($input['car_admin_password']);
        App\Model\Car\CarAdmin::create($input);
        return back();
    }
    public function addCity(Request $request){
        App\Model\City\CreateCity::create($request->all());
        return back();
    }
    public function addTripDriver(Request $request){
        App\Model\Trip\TripDriver::create($request->all());
        return back();
    }
    public function addTripSolo(Request $request){
        App\Model\Trip\TripSolo::create($request->all());
        return back();
    }
    public function addCarType(Request $request){
        App\Model\Car\CarType::create($request->all());
        return back();
    }


    public function getcars(){
        $allcars = Car::with('car_admins123', 'drivers', 'create_cities', 'car_type123')->get();

        /*dd($allcars);*/
        return view('AdminPanel.pages.getCars')->with([
            'allcars' => $allcars
        ]);
    }
    public function getcities(){
        $allcities = App\Model\City\CreateCity::all();
        return view('AdminPanel.pages.getcities')->with([
            'allcities' => $allcities
        ]);
    }
    public function getcustomers(){
        $allcustomers = App\Model\Customer\Customer::all();
        return view('AdminPanel.pages.getcustomers')->with([
            'allcustomers' => $allcustomers
        ]);
    }
    public function getdrivers(){
        $alldrivers = App\Model\Car\Driver::with('cars123')->get();
        return view('AdminPanel.pages.getdrivers')->with([
            'alldrivers' => $alldrivers
        ]);
    }
    public function getcaradmins(){
        $allcaradmins = App\Model\Car\CarAdmin::all();
        return view('AdminPanel.pages.getcaradmins')->with([
            'allcaradmins' => $allcaradmins
        ]);
    }
    public function getcartypes(){
        $allcartypes = App\Model\Car\CarType::all();
        return view('AdminPanel.pages.getcartype')->with([
            'allcartypes' => $allcartypes
        ]);
    }

}
