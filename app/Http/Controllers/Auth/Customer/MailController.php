<?php

namespace App\Http\Controllers\Auth\Customer;

use App\Mail\VerificationMail;
use App\Model\Car\CarAdmin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;

class MailController extends Controller
{
    protected $verifier = '';
    public function authentication(Request $request){
        $customer = CarAdmin::where('customer_uuid', $request->customer_uuid)->first();
        $verifier = substr(str_shuffle('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'),0, 50);
        $customer->verifier = $verifier;
        $customer->save();
        $to = $request->customer_email;
        $email = encrypt($to);
        $name = $request->customer_name;
        Mail::to($to)->send(new VerificationMail($verifier, $name, $email));
        return redirect()->route('customer.emailresend');
    }
    public function emailresend(){
        return view('mail.resendemail');
    }
}
