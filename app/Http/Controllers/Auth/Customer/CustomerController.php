<?php
namespace App\Http\Controllers\Auth\Customer;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use App;
use App\Model\Customer\Customer;
use App\Http\Controllers\Controller;
class CustomerController extends Controller
{
    use ValidatesRequests;
    public function __construct()
    {
        $this->middleware('auth:customer',['only' => 'index','edit']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('auth.customer.dashboard');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('auth.customer.auth.register');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // validate the data
        $this->validate($request, [
            'customer_uuid'          => 'required',
            'customer_name'          => 'required',
            'customer_email'         => 'required',
            'customer_password'      => 'required'
        ]);
        // store in the database
        $customers = new Customer;
        $customers->id = $request->customer_uuid;
        $customers->customer_name = $request->customer_name;
        $customers->customer_email = $request->customer_email;
        $customers->customer_phone = $request->customer_phone;
        $customers->customer_license = $request->customer_license;
        $customers->customer_city = $request->customer_city;
        $customers->customer_lat = $request->customer_lat;
        $customers->customer_long = $request->customer_long;
        $customers->customer_altitude = $request->customer_altitude;
        $customers->customer_password=bcrypt($request->customer_password);
        $customers->save();
        return redirect()->route('customer.auth.login');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function verifier($verifier, $email){
        $customerdata = Customer::where('customer_email', decrypt($email))->first();
        if ($verifier == $customerdata->verifier){
            $customerdata->email_verified_at = Carbon::now();
            $customerdata->save();
            return redirect()->route('customer.dashboard');
        }
        else{
            return redirect()->route('customer.dashboard');
        }
    }
}
