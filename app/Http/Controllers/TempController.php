<?php

namespace App\Http\Controllers;

use App\Model\Car\Car;
use App\Model\Customer\Customer;
use App\Model\Temp\TempCustomer;
use Carbon\Carbon;

/*use http\Env\Response;*/

//use http\Client\Response;
use Illuminate\Http\Request;
use Webpatser\Uuid\Uuid;

class TempController extends Controller
{
    public function Store(Request $request)
    {
        $tempcustomerexists = TempCustomer::where('temp_phone',$request->temp_phone)->first();
        if(!$tempcustomerexists){
            $tempcustomer = new TempCustomer();
            $tempcustomer->temp_phone = $request->temp_phone;
            $successotp = $this->sendSMS($tempcustomer);
            $tempcustomer->save();
            return response()->json([
                'temp_phone' => $successotp
            ]);
        }
        else{
            $successotp = $this->sendSMS($tempcustomerexists);
            $tempcustomerexists->save();
            return response()->json([
                'temp_phone' => $successotp
            ]);
        }



    }

    public function sendSMS(TempCustomer $tempcustomer)
    {
        //Your authentication key
        $authKey = "143565AIE7jf1Mb5abd98a0";
        $mobileNumber = $tempcustomer->temp_phone;
        //Multiple mobiles numbers separated by comma
//        $mobileNumber = "8172071965";

        //Sender ID,While using route4 sender id should be 6 characters long.
        $senderId = "TECION";
//        $receiverId = $tempcustomer->id;

        /*$name = $tempcustomer->admin_name;*/

        $otp = rand(1000, 9999);
//        $otp = 4085;
        //Your message to send, Add URL encoding here.
        $message = urlencode("Hello, Your OTP for Cabee is " . $otp);

        //Define route
        $route = "4";

        //Prepare you post parameters
        $postData = array(
            'authkey' => $authKey,
            'mobiles' => $mobileNumber,
            'message' => $message,
            'sender' => $senderId,
            'route' => $route
        );

        //API URL
        $url = "http://api.msg91.com/api/sendhttp.php";

        // init the resource
        $ch = curl_init();
        curl_setopt_array($ch, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $postData
            //,CURLOPT_FOLLOWLOCATION => true
        ));


//Ignore SSL certificate verification
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);

        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);


//get response
        $output = curl_exec($ch);

//Print error if any
        if (curl_errno($ch)) {
            echo 'error:' . curl_error($ch);
        }

        curl_close($ch);
//        echo $output;
        /*$encryptedOTP = encrypt($otp);
        $admin = Customer::where('admin_phone', $mobileNumber)->first();
        $admin->otp_verifier = $encryptedOTP;
        $admin->save();*/
        /*$customer = new Customer();
        $customer->otp_verifier = encrypt($otp);*/
        $tempcustomer->otp_verifier = $otp;
        $tempcustomer->otp_sent_at = Carbon::now();
        /*return response()->json([
            'tempphone' => $mobileNumber
        ]);*/
        return $mobileNumber;
    }

    public function otpverifyapi(Request $request)
    {

        $receiverNo = $request->temp_phone;
        $tempcustomer = TempCustomer::where('temp_phone', $receiverNo)->first();
        // dd($tempcustomer);
//        $tempcustomer = TempCustomer::all();
//        dd($tempcustomer);
        $decryptedOTP = $tempcustomer->otp_verifier;
        $otpEntered = $request->otpEntered;
        if ($otpEntered == $decryptedOTP) {
            $currentTime = Carbon::now();
            $tempcustomer->temp_otp_verified_at = $currentTime;
            $tempcustomer->save();
            $customer = new Customer();
            $customer->customer_phone = $tempcustomer->temp_phone;
            $customer->otp_verifier = $otpEntered;
            $customer->phone_verified_at = $currentTime;
            /*            $customer->id = (string) Uuid::generate('4');*/
            $customer->save();
            $tempcustomer->delete();
            $token = $customer->createToken('MyApp')->accessToken;
            $name = 'Cabee User';
            $customer_phone = $customer->customer_phone;
            return response()->json([
            	'token' => $token,
            	'name' => $name,
            	'customer_phone' => $customer_phone
            	], 200);
        } else {
            return response()->json([
                'failed'
            ], 403);
        }
    }
}
