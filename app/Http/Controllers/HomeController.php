<?php

namespace App\Http\Controllers;

use App\Model\Car\Car;
use App\Model\Car\CarLocation;
use App\Model\City\CreateCity;
use App\Model\Customer\Customer;
use App\Model\Search;
use Carbon\Carbon;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;


class HomeController extends Controller
{
    use ValidatesRequests;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    /*public function __construct()
    {
    $this->middleware('auth');
    }*/

    /**
     * Show the application dashboard.
     */
    public function index()
    {
        return view('home');
    }

    public function updatelocation(Request $request, CarLocation $carLocation)
    {
        $request->validate([
            'car_id' => 'nullable',
            'car_lat' => 'nullable',
            'car_long' => 'nullable',
            'car_altitude' => 'nullable',
            'is_moving' => 'nullable'
        ]);

        $triggeredTime = Carbon::now();

        $oldTime = new Carbon($carLocation->updated_at);

        $differenceTime = $oldTime->diffInSeconds($triggeredTime);

        if ($differenceTime >= 100) {
            $carLocation->update($request->all());
        }


        return response()->json([
            'message' => 'Great success! Task updated',
            'carLocation' => $carLocation,
            'TimeDifference' => $differenceTime
        ]);
    }

    public function addlocation(Request $request)
    {
        $request->validate([
            'car_id' => 'required',
            'car_lat' => 'required',
            'car_long' => 'required',
            'car_altitude' => 'required'
        ]);

        $carLocation = CarLocation::create($request->all());

        return response()->json([
            'message' => 'Great! Location Added',
            'carLocation' => $carLocation
        ]);
    }

    public function showLocation(CarLocation $carLocation)
    {
        return $carLocation;
    }

    public function addcar(Request $request)
    {
        /*$request->validate([
        'car_number' => 'required',
        'car_admin_uuid' => 'required',
        'car_type' => 'required',
        'car_model' => 'required',
        'car_fuel_type' => 'required',
        'driver_uuid' => 'required',
        'car_capacity' => 'required',
        'car_fare_per_hr' => 'required',
        'car_fare_per_km' => 'required',
        'car_image' => 'required',
        'car_city' => 'required',
        'car_altitude' => 'required',
        'is_moving' => 'required'
        ]);*/

        $task = Car::create($request->all());

        return response()->json([
            'message' => 'Great success! New task created',
            'task' => $task
        ]);
    }

    /*public function getRadius(Request $request){

    }*/

    public function getNearestCars(Request $request, Customer $customer)
    {
        $request->validate([
            'customer_lat' => 'required',
            'customer_long' => 'required',
            'customer_city' => 'required'
        ]);

        $cityid = $request->customer_city;
        $cars = CreateCity::with('cars123')->find($cityid);
        $collection = collect($cars);
//        $plucked = $collection->get('cars123');
        return response()->json([
            'message' => 'All Cars in Siliguri',
            'cars' => $collection
        ]);


        /*$cars = CreateCity::with('cars')->find('1');*/
        /*$cars = CreateCity::with('cars123')->find('1');*/
//$cars = Car::with('create_cities')->where('car_city', '1');
//return $cars->cars;
/////*$collection = collect($cars);*/
//        $plucked = $cars->pluck('name', 'id');
//        $plucked = $cars->map(function);
        /*$plucked= $collection->get('cars123');*/
        /*return $plucked->all();*/

//        dd($plucked);


        /* $carlastlocation = $cars->sortBy('car_locations.updated_at')->groupBy('car_locations.updated_at');
        $customer_lat = $customer->customer_lat;
        $customer_long = $customer->customer_long;
        $carlastlat = $carlastlocation->car_lat;
        $carlastlong = $carlastlocation->car_long;
        /*dd($cars);*/
        /*foreach($cars->car_locations123 as $car){
        dd($car);
        }*/
        /*return response()->json([
        'message' => 'Great',
        'cars' => $cars
        ]);*/
        /* foreach ($cars as $car) {
        foreach ($car->car_locations as $car_location) {
        return response()->json([
        'message' => 'Great success! New task created',
        'cars' => $car
        ]);
        $carsArray = collect([
        'car_id' => $car->car_id,
        'car_city' => $car->car_city,
        'car_model' => $car->car_model
        ]);
        }
        }
        return response()->json([
        'message' => 'Greate',
        'cars' => $carsArray
        ]);*/
        /*$carsCollection = collect($cars);*/

        /*$filtered = $carsCollection->reject(function ($value){
        return $value = 'siliguri';
        });*/
        /*$users = User::all();*/
        /*$key = collect($customer);*/
        /*$customercity = $customer->customer_city;
        $siliguriCars = $cars->map(function ($item, $key) {
        return $item;
        });*/

        /*$filtered = $cars->filter(function ($value, $key) {
        return $value->isNear();
        });

        $filtered->all();*/

        /*$cars->reject()->all();*/
        /*$nearestCars = $cars->where('price', 100);*/
//        dd($cars);
        /*dd($carlastlocation);*/
        /*$carid = 1;
        $filter = $cars->filter(function($value, $key) use($carid) {
        if ($value['car_id'] == $carid) {
        return true;
        }
        else{
        return false;
        }
        });*/
        /*return response()->json([
        'message' => 'All Cars in Siliguri',
        'siliguri-cars' => $plucked
        ]);*/

        /*$car_lat = $car_location->car_lat;
        $car_long = $car_location->car_long;

        if (acos(sin($customer_lat) * sin($car_lat) + cos($customer_lat) * cos($car_lat) * cos($car_long - $customer_long)) * 6371 <= 1) {
        return response()->json([
        'message' => 'This is another car',
        'car' => $car
        ]);
        }*/

    }

    /*public function isNear(Customer $customer, $value){
    if ()
    }*/

    public function getCityCars(Request $request, CreateCity $city)
    {
        $cityid = $request->CreateCity;
        $cars = CreateCity::with('cars123')->find($cityid);
        $collection = collect($cars);
        $plucked = $collection->get('cars123');
        return response()->json($plucked);
    }
    public function getCars(Request $request){
        $cars = Car::with('create_cities','car_type123','car_admins123','drivers')->get();
        return response()->json([
            'cars' => $cars
        ]);
    }
    public function searchMethod(Request $request){
        $search = Search::create($request->all());
        return response()->json([
            'data' => $search
        ]);
    }
}
