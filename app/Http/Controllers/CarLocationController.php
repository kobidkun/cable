<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CarLocationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CarLocation  $carLocation
     * @return \Illuminate\Http\Response
     */
    public function show(CarLocation $carLocation)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CarLocation  $carLocation
     * @return \Illuminate\Http\Response
     */
    public function edit(CarLocation $carLocation)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CarLocation  $carLocation
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CarLocation $carLocation)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CarLocation  $carLocation
     * @return \Illuminate\Http\Response
     */
    public function destroy(CarLocation $carLocation)
    {
        //
    }
}
