<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
class SmsController extends Controller
{
    public function index(){
        return view('receiver');
    }
    public function sendSMS(Request $request){
        //Your authentication key
        $authKey = "143565AIE7jf1Mb5abd98a0";
        $mobileNumber = $request->number;
        //Multiple mobiles numbers separated by comma
//        $mobileNumber = "8172071965";

        //Sender ID,While using route4 sender id should be 6 characters long.
        $senderId = "TECION";

        $name = $request->name;

        $otp = rand(1000, 9999);
        //Your message to send, Add URL encoding here.
        $message = urlencode("Hello ".$name.", Your OTP is ".$otp);

        //Define route
        $route = "4";

        //Prepare you post parameters
        $postData = array(
            'authkey' => $authKey,
            'mobiles' => $mobileNumber,
            'message' => $message,
            'sender' => $senderId,
            'route' => $route
        );

        //API URL
        $url="http://api.msg91.com/api/sendhttp.php";

        // init the resource
        $ch = curl_init();
        curl_setopt_array($ch, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $postData
            //,CURLOPT_FOLLOWLOCATION => true
        ));


//Ignore SSL certificate verification
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);


//get response
        $output = curl_exec($ch);

//Print error if any
        if(curl_errno($ch))
        {
            echo 'error:' . curl_error($ch);
        }

        curl_close($ch);
//        echo $output;
        $encryptedOTP = encrypt($otp);
        return view('otp-verify')->with('encryptedOTP',$encryptedOTP);
//        return view('otp-verify')->with($otp);
    }
    public function otp(Request $request){
        $decryptedOTP = decrypt($request->encryptedOTP);
        $otpEntered = $request->otpEntered;
        if($otpEntered == $decryptedOTP){
            return view('otp-success');
        }
        else{
            return view('otp-fail');
        }
    }
}
