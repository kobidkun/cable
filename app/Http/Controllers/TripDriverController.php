<?php

namespace App\Http\Controllers;

use App\Model\Car\Car;
use App\Model\Car\CarType;
use App\Model\Trip\TripDriver;
use http\Env\Response;
use Illuminate\Http\Request;

class TripDriverController extends Controller
{
    public function addTripDriver(Request $request){
        $request->validate([
            'customer_uuid' => 'required',
            'driver_uuid' => 'required',
            'pickup_lat' => 'required',
            'pickup_long' => 'required',
            'pickup_address' => 'required',
            'drop_lat' => 'required',
            'drop_long' => 'required',
            'total_distance' => 'required'
        ]);

        $task = TripDriver::create($request->all());

        return response()->json([
            'message' => 'Great success! New trip added',
            'task' => $task
        ]);
    }
    /*public function getTotalFare(Request $request, CarType $carType){
        $request->validate([
            'id' => 'required',
            'car_type_name' => 'required',
            'fare_per_km' => 'required',
            'fare_per_hr' => 'required'
        ]);
        $time = $
        $cartypedetails = CarType::find($carType);
        $totalFare = $request->
    }*/
    public function bookingConfirm(Request $request, Car $car){
        $cars = Car::with('car_admins123', 'drivers', 'trip_drivers', 'trip_solos', 'create_cities', 'car_type123')->find($request->Car);
        return response()->json([
            'cardetails' => $cars
        ]);
    }
    
}
