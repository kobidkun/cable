<?php
namespace App\Mail;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
class VerificationMail extends Mailable
{
    use Queueable, SerializesModels;
    public $verifier, $username, $useremail;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($verifier, $name, $email)
    {
        $this->verifier = $verifier;
        $this->username = $name;
        $this->useremail = $email;
    }


    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('akash@tecions.com', 'Akash')
            ->subject('Your Cab is confirmed')
            ->markdown('mail.authentication')
            ->with([
                'name' => $this->username,
                'link' => url('/').'/admin/verifier/'.$this->verifier.'/'.$this->useremail
            ]);
    }
}
