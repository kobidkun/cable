@component('mail::message')
    Hello **{{$name}}**,  {{-- use double space for line break --}}
    You have signed up at Uber Zoomcar App!
    Please Verify your connection by
    <a href="{{$link}}">Clicking here</a>



    Sincerely,
    Team Uber Zoomcar
@endcomponent
