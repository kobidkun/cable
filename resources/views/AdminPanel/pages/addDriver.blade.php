@extends('auth.admin.layouts.app')
@section('content')
    <div class="container-fluid">
        <div class="row content">
            <div class="col-sm-3 sidenav hidden-xs">
                <h2>Add Driver</h2>
                {{--<ul class="nav nav-pills nav-stacked">
                    <li><a href="{{route('tripPage')}}">Show Cars</a></li>
                    <li><a href="{{route('hotelPage')}}">Add Car</a></li>
                    <li><a href="{{route('tripPage')}}">Show Cars by Cities</a></li>
                    <li><a href="{{route('carPage')}}">Show Cities</a></li>
                    <li><a href="{{route('contactPage')}}">Add City</a></li>
                    <li><a href="{{route('carPage')}}">Show Trip with Drivers</a></li>
                    <li><a href="{{route('contactPage')}}">Add Trip with Driver</a></li>
                    --}}{{--<li><a href="{{route('carPage')}}">Show Cities</a></li>
                    <li><a href="{{route('contactPage')}}">Add City</a></li>--}}{{--
                    <li><a href="{{route('carPage')}}">Show Drivers</a></li>
                    <li><a href="{{route('contactPage')}}">Add Driver</a></li>
                    <li><a href="{{route('carPage')}}">Show Customers</a></li>
                    <li><a href="{{route('contactPage')}}">Add Customers</a></li>
                    <li><a href="{{route('carPage')}}">Show Car Admins</a></li>
                    <li><a href="{{route('contactPage')}}">Add Car Admin</a></li>
                    --}}{{--<li class="active"><a href="{{route('addTaxi.get')}}">Add Taxi</a></li>
                    <li><a href="{{route('getTaxi')}}">All Taxis</a></li>--}}{{--
                    <li><a class="dropdown-item" href="{{ route('logout') }}"
                           onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                            {{ __('Logout') }}
                        </a></li>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </ul>--}}
            </div>
            <br>
            {{--<h1>Add Taxi</h1>--}}
            <div class="col-sm-6">
                <div class="well">
                    <form action="{{route('admin.adddriver.post')}}" method="post">
                        @csrf
                        <input type="hidden" name="id" value="{{Uuid::generate()->string}}">

                        {{--<div class="form-group">
                            <select class="form-control" name="car_city">
                                <option>Car City</option>
                                @foreach ($cities as $city)
                                    <option value="{{$city->id}}">{{$city->name}}</option>
                                @endforeach
                            </select>
                        </div>--}}
                        <div class="form-group">
                            <input type="text" class="form-control" id="driver_name" placeholder="Driver Name" name="driver_name">
                        </div>
                        <div class="form-group">
                            <select class="form-control" name="car_id">
                                <option>Car Assigned</option>
                                @foreach($allcars as $car)
                                    <option value="{{$car->id}}">{{$car->car_number}} ({{$car->car_model}})</option>
                                    @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <input type="number" class="form-control" id="driver_phone" placeholder="Driver Phone" name="driver_phone" required>
                        </div>
                        <div class="form-group">
                            <input type="email" class="form-control" id="driver_email" placeholder="Driver Email" name="driver_email" required>
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" id="driver_license" placeholder="Driver License" name="driver_license" required>
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control" id="driver_password" placeholder="Driver Password" name="driver_password" required>
                        </div>

                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
