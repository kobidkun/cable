@extends('auth.admin.layouts.app')
@section('content')
    <div class="container-fluid">
        <div class="row content">
            <div class="col-sm-3 sidenav hidden-xs">
                <h2>Add Car</h2>
                {{--<ul class="nav nav-pills nav-stacked">
                    <li><a href="{{route('tripPage')}}">Show Cars</a></li>
                    <li><a href="{{route('hotelPage')}}">Add Car</a></li>
                    <li><a href="{{route('tripPage')}}">Show Cars by Cities</a></li>
                    <li><a href="{{route('carPage')}}">Show Cities</a></li>
                    <li><a href="{{route('contactPage')}}">Add City</a></li>
                    <li><a href="{{route('carPage')}}">Show Trip with Drivers</a></li>
                    <li><a href="{{route('contactPage')}}">Add Trip with Driver</a></li>
                    --}}{{--<li><a href="{{route('carPage')}}">Show Cities</a></li>
                    <li><a href="{{route('contactPage')}}">Add City</a></li>--}}{{--
                    <li><a href="{{route('carPage')}}">Show Drivers</a></li>
                    <li><a href="{{route('contactPage')}}">Add Driver</a></li>
                    <li><a href="{{route('carPage')}}">Show Customers</a></li>
                    <li><a href="{{route('contactPage')}}">Add Customers</a></li>
                    <li><a href="{{route('carPage')}}">Show Car Admins</a></li>
                    <li><a href="{{route('contactPage')}}">Add Car Admin</a></li>
                    --}}{{--<li class="active"><a href="{{route('addTaxi.get')}}">Add Taxi</a></li>
                    <li><a href="{{route('getTaxi')}}">All Taxis</a></li>--}}{{--
                    <li><a class="dropdown-item" href="{{ route('logout') }}"
                           onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                            {{ __('Logout') }}
                        </a></li>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </ul>--}}
            </div>
            <br>
            {{--<h1>Add Taxi</h1>--}}
            <div class="col-sm-6">
                <div class="well">
                    <form action="{{route('admin.addcar.post')}}" method="post">
                        @csrf
                        <div class="form-group">
                            <input type="text" class="form-control" id="car_number" placeholder="Car Number" name="car_number" required>
                        </div>
                        <div class="form-group">
                            <select class="form-control" name="car_city">
                                <option>Car City</option>
                                @foreach ($cities as $city)
                                    <option value="{{$city->id}}">{{$city->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" id="car_lat" placeholder="Car Latitude" name="car_lat">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" id="car_long" placeholder="Car Longitude" name="car_long" required>
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" id="car_altitude" placeholder="Car Altitude" name="car_altitude" required>
                        </div>
                        <div class="form-group">
                            <select class="form-control" name="is_moving">
                                <option>Is it Moving?</option>
                                <option value="0">No</option>
                                <option value="1">Yes</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <select class="form-control" name="car_admin_uuid">
                                <option>Car Admin</option>
                                @foreach ($caradmins as $caradmin)
                                        <option value="{{$caradmin->id}}">{{$caradmin->car_admin_name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <select class="form-control" name="car_type">
                                <option>Car Type</option>
                                @foreach ($cartypes as $cartype)
                                    <option value="{{$cartype->id}}">{{$cartype->car_type_name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" id="car_model" placeholder="Car model" name="car_model">
                        </div>
                        <div class="form-group">
                            <select class="form-control" name="car_fuel_type">
                                <option>Car Fuel Type</option>
                                <option value="petrol">Petrol</option>
                                <option value="diesel">Diesel</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <select class="form-control" name="car_capacity">
                                <option>Car Capacity</option>
                                @for ($i = 1; $i <= 10; $i++)
                                   <option value="{{$i}}">{{$i}}</option>
                                @endfor
                            </select>
                        </div>
                        {{--<div class="form-group">
                            <input type="text" class="form-control" id="distance" placeholder="Car Image" name="car_image">
                        </div>--}}
                        <div class="form-group">
                            <select class="form-control" name="driver_uuid">
                                <option>Driver</option>
                                @foreach ($drivers as $driver)
                                    <option value="{{$driver->id}}">{{$driver->driver_name}}</option>
                                @endforeach
                            </select>
                        </div>
                        {{--<div class="form-group">
                            <label for="duration5">Duration</label>
                            <input type="text" class="form-control" id="duration5" name="time">
                            <script>
                                $(function () {
                                    $('#duration5').durationPicker({
                                        showDays: false,
                                        onChanged: function (newVal) {
                                            $('#duration-label5').text(newVal);
                                        }
                                    });
                                });
                            </script>
                        </div>--}}
                        {{--<div class="form-group">
                            <label for="description" style="vertical-align: top">Description (Optional)</label>
                            --}}{{--                            <input type="text" class="form-control" id="ending_point" placeholder="Enter Ending Point" name="ending_point">--}}{{--
                            <textarea id="description" placeholder="Description" name="description"></textarea>
                        </div>--}}
                        {{--<div class="form-group form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="checkbox" name="remember"> Remember me
                            </label>
                        </div>--}}
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
