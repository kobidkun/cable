@extends('auth.admin.layouts.app')
@section('content')
    <div class="container-fluid">
        <div class="row content">
           {{-- <div class="col-sm-3 sidenav hidden-xs">
                --}}{{--<ul class="nav nav-pills nav-stacked">
                    <li><a href="{{route('tripPage')}}">Trips</a></li>
                    <li><a href="{{route('hotelPage')}}">Hotels</a></li>
                    <li><a href="{{route('carPage')}}">Cars</a></li>
                    <li><a href="{{route('contactPage')}}">Contacts</a></li>
                    <li><a href="{{route('quickEnquiryPage')}}">Quick Enquiries</a></li>
                    <li><a href="{{route('paymentPage')}}">Payments</a></li>
                    <li><a href="{{route('addTaxi.get')}}">Add Taxi</a></li>
                    <li class="active"><a href="{{route('getTaxi')}}">All Taxis</a></li>
                    <li><a class="dropdown-item" href="{{ route('logout') }}"
                           onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                            {{ __('Logout') }}
                        </a></li>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </ul>--}}{{--
            </div>
            <br>--}}
            <h1>All Cars</h1>

            <div class="col-sm-9">
                <div style="text-align: center"><button class="btn-default">Add Car</button></div>

                <div class="well">
                    <table id="simple-datatable-example" class="display" style="width:100%">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>car number</th>
                            <th>car city</th>
                            <th>car lat</th>
                            <th>car long</th>
                            <th>car altitude</th>
                            <th>is moving</th>
                            <th>car admin name</th>
                            <th>car type name</th>
                            <th>car model</th>
                            <th>car fuel_type</th>
                            <th>car capacity</th>
                            <th>driver name</th>
{{--                            <th>Delete</th>--}}
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($allcars as $car)
                            <tr>
                                <td data-column="ID">{{$car->id}}</td>
                                <td data-column="car number">{{$car->car_number}}</td>
                                <td data-column="car city">{{$car->create_cities->car_city}}</td>
                                <td data-column="car lat">{{$car->car_lat}}</td>
                                <td data-column="car long">{{$car->car_long}}</td>
                                <td data-column="car altitude">{{$car->car_altitude}}</td>
                                <td data-column="is moving">{{$car->is_moving}}</td>
                                <td data-column="car admin name">{{$car->car_admins123->car_admin_name}}</td>
                                <td data-column="car type name">{{$car->car_type123->car_type_name}}</td>
                                <td data-column="car model">{{$car->car_model}}</td>
                                <td data-column="car fuel_type">{{$car->car_fuel_type}}</td>
                                <td data-column="car capacity">{{$car->car_capacity}}</td>
                                <td data-column="driver name">{{$car->drivers->driver_name}}</td>
{{--                                <td data-column="Delete"><button type="button" onclick="window.location.href='{{route('getCityCars',$car->id)}}'">Delete</button> </td>--}}
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
