@extends('auth.admin.layouts.app')
@section('content')
    <div class="container-fluid">
        <div class="row content">
            {{-- <div class="col-sm-3 sidenav hidden-xs">
                 --}}{{--<ul class="nav nav-pills nav-stacked">
                     <li><a href="{{route('tripPage')}}">Trips</a></li>
                     <li><a href="{{route('hotelPage')}}">Hotels</a></li>
                     <li><a href="{{route('carPage')}}">Cars</a></li>
                     <li><a href="{{route('contactPage')}}">Contacts</a></li>
                     <li><a href="{{route('quickEnquiryPage')}}">Quick Enquiries</a></li>
                     <li><a href="{{route('paymentPage')}}">Payments</a></li>
                     <li><a href="{{route('addTaxi.get')}}">Add Taxi</a></li>
                     <li class="active"><a href="{{route('getTaxi')}}">All Taxis</a></li>
                     <li><a class="dropdown-item" href="{{ route('logout') }}"
                            onclick="event.preventDefault();
                                                      document.getElementById('logout-form').submit();">
                             {{ __('Logout') }}
                         </a></li>
                     <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                         @csrf
                     </form>
                 </ul>--}}{{--
             </div>
             <br>--}}
            <h1>All Cars</h1>

            <div class="col-sm-12">
                <div style="text-align: center"><button class="btn-default">Add Car</button></div>

                <div class="well">
                    <table id="simple-datatable-example" class="display" style="width:100%">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>car_admin_name</th>
                            <th>car_admin_email</th>
                            <th>car_admin_phone</th>
{{--                            <th>car_admin_password</th>--}}
{{--                            <th>Delete</th>--}}
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($allcaradmins as $caradmin)
                            <tr>
                                <td data-column="ID">{{$caradmin->id}}</td>
                                <td data-column="car number">{{$caradmin->car_admin_name}}</td>
                                <td data-column="car city">{{$caradmin->car_admin_email}}</td>
                                <td data-column="car lat">{{$caradmin->car_admin_phone}}</td>
{{--                                <td data-column="car long">{{$caradmin->car_admin_password}}</td>--}}
{{--                                <td data-column="Delete"><button type="button" onclick="window.location.href='{{route('getCityCars',$caradmin->id)}}'">Delete</button> </td>--}}
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
