@extends('auth.admin.layouts.app')
@section('content')
    <div class="container-fluid">
        <div class="row content">
            {{-- <div class="col-sm-3 sidenav hidden-xs">
                 --}}{{--<ul class="nav nav-pills nav-stacked">
                     <li><a href="{{route('tripPage')}}">Trips</a></li>
                     <li><a href="{{route('hotelPage')}}">Hotels</a></li>
                     <li><a href="{{route('carPage')}}">Cars</a></li>
                     <li><a href="{{route('contactPage')}}">Contacts</a></li>
                     <li><a href="{{route('quickEnquiryPage')}}">Quick Enquiries</a></li>
                     <li><a href="{{route('paymentPage')}}">Payments</a></li>
                     <li><a href="{{route('addTaxi.get')}}">Add Taxi</a></li>
                     <li class="active"><a href="{{route('getTaxi')}}">All Taxis</a></li>
                     <li><a class="dropdown-item" href="{{ route('logout') }}"
                            onclick="event.preventDefault();
                                                      document.getElementById('logout-form').submit();">
                             {{ __('Logout') }}
                         </a></li>
                     <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                         @csrf
                     </form>
                 </ul>--}}{{--
             </div>
             <br>--}}
            <h1>All Car Types</h1>

            <div class="col-sm-12">
                <div style="text-align: center"><button class="btn btn-outline-primary" onclick="window.location.href='{{route('admin.addCarTypePage')}}'">Add a New Car Type</button></div>

                <div class="well">
                    <table id="simple-datatable-example" class="display" style="width:100%">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>car_type_name</th>
                            <th>fare_per_km</th>
                            <th>fare_per_hr</th>
                            {{--                            <th>car_admin_password</th>--}}
{{--                            <th>Delete</th>--}}
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($allcartypes as $cartype)
                            <tr>
                                <td data-column="ID">{{$cartype->id}}</td>
                                <td data-column="car number">{{$cartype->car_type_name}}</td>
                                <td data-column="car city">{{$cartype->fare_per_km}}</td>
                                <td data-column="car lat">{{$cartype->fare_per_hr}}</td>
                                {{--                                <td data-column="car long">{{$cartype->car_admin_password}}</td>--}}
{{--                                <td data-column="Delete"><button type="button" class="btn btn-outline-danger" onclick="window.location.href='{{route('getCityCars',$cartype->id)}}'">Delete</button> </td>--}}
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
