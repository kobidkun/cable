@extends('AdminPanel.base')
@section('PageTitle', 'Payments')
@section('content')
    <div class="container-fluid">
        <div class="row content">
            <div class="col-sm-3 sidenav hidden-xs">
                <h2>Destinasia</h2>
                <ul class="nav nav-pills nav-stacked">
                    <li><a href="{{route('tripPage')}}">Trips</a></li>
                    <li><a href="{{route('hotelPage')}}">Hotels</a></li>
                    <li><a href="{{route('carPage')}}">Cars</a></li>
                    <li><a href="{{route('contactPage')}}">Contacts</a></li>
                    <li><a href="{{route('quickEnquiryPage')}}">Quick Enquiries</a></li>
                    <li><a href="{{route('paymentPage')}}">Payments</a></li>
                    <li><a href="{{route('addTaxi.get')}}">Add Taxi</a></li>
                    <li class="active"><a href="{{route('getTaxi')}}">All Taxis</a></li>
                    <li><a class="dropdown-item" href="{{ route('logout') }}"
                           onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                            {{ __('Logout') }}
                        </a></li>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </ul>
            </div>
            <br>
            <h1>All Taxis</h1>
            <div class="col-sm-9">
                <div class="well">
                    <table id="simple-datatable-example" class="display" style="width:100%">
                        <thead>
                        <tr>
                            <th>Id</th>
                            <th>Starting Point</th>
                            <th>Ending Point</th>
                            <th>Distance</th>
                            <th>Time</th>
                            <th>Description</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($allTaxis as $taxi)
                            <tr>
                                <td data-column="Id">{{$taxi->id}}</td>
                                <td data-column="Starting Point">{{$taxi->starting_point}}</td>
                                <td data-column="Ending Point">{{$taxi->ending_point}}</td>
                                <td data-column="Distance">{{$taxi->distance}}</td>
                                <td data-column="Time">{{$taxi->hour}} hr, {{$taxi->min}} mins</td>
                                <td data-column="Arrival Date">{{$taxi->description}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
