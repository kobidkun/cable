@extends('AdminTemplate.base')
@section('content')
    <div class="page-wrapper">
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h3 class="text-themecolor">Add-Order</h3>
            </div>
            <div class="col-md-7 align-self-center">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                    <li class="breadcrumb-item">Order Details</li>
                    <li class="breadcrumb-item active">Add Order</li>
                </ol>
            </div>
            <div class="">
                <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card card-outline-info">
                        <div class="card-header">
                            <h4 class="m-b-0 text-white">Add Order Details</h4>
                        </div>
                        <div class="card-body">
                            <form action="{{route('addorderdetails')}}" class="form-horizontal" method="post">
                                @csrf
                                <div class="form-body">

                                    <hr class="m-t-0 m-b-40">
                                    <div class="row">
                                        <div class="col-md-6" style="display: none;">
                                            <div class="form-group row">
                                                <label class="control-label text-right col-md-3">Admin_id</label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" placeholder="" name="admin_id" value="{{ Auth::user()->admin_id}}">
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                        <div class="col-md-6">
                                            <div class="form-group  row">
                                                <label class="control-label text-right col-md-3">Customer id</label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" placeholder="" name="customer_id" value="">
                                                </div>
                                            </div>
{{--                                            <div class="container box">--}}
{{--                                                <h3 align="center">Ajax Autocomplete Textbox in Laravel using JQuery</h3><br />--}}

{{--                                                <div class="form-group">--}}
{{--                                                    <input type="text" name="country_name" id="country_name" class="form-control input-lg" placeholder="Enter Customer Name" />--}}
{{--                                                    <div id="countryList">--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}
{{--                                                {{ csrf_field() }}--}}
{{--                                            </div>--}}




                                        </div>
                                        <!--/span-->
                                    </div>
                                    <!--/row-->
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group row">
                                                <label class="control-label text-right col-md-3">Order Name</label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" placeholder="" name="order_name" value="">
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                        <div class="col-md-6">
                                            <div class="form-group row">
                                                <label class="control-label text-right col-md-3">Order Quantity</label>
                                                <div class="col-md-9">
                                                    <input type="number" class="form-control" placeholder="" name="order_quantity" value="">
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                    </div>
                                    <!--/row-->
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group row">
                                                <label class="control-label text-right col-md-3">Order price</label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" placeholder="" name="order_price" value="">
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                    </div>
                                    <hr>
                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="row">
                                                    <div class="col-md-offset-3 col-md-9">
                                                        <button type="submit" class="btn btn-success">Submit</button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6"> </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{--experiment code--}}
{{--    <script>--}}
{{--        $(document).ready(function(){--}}

{{--            $('#country_name').keyup(function(){--}}
{{--                var query = $(this).val();--}}
{{--                if(query != '')--}}
{{--                {--}}
{{--                    var _token = $('input[name="_token"]').val();--}}
{{--                    $.ajax({--}}
{{--                        url:"{{ route('autocomplete.fetch') }}",--}}
{{--                        method:"POST",--}}
{{--                        data:{query:query, _token:_token},--}}
{{--                        success:function(data){--}}
{{--                            $('#countryList').fadeIn();--}}
{{--                            $('#countryList').html(data);--}}
{{--                        }--}}
{{--                    });--}}
{{--                }--}}
{{--            });--}}

{{--            $(document).on('click', 'li', function(){--}}
{{--                $('#country_name').val($(this).text());--}}
{{--                $('#countryList').fadeOut();--}}
{{--            });--}}

{{--        });--}}
{{--    </script>--}}




    {{--end experiment code--}}


@stop