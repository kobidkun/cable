@extends('AdminTemplate.base')
@section('content')
    <div class="page-wrapper">
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h3 class="text-themecolor">Add-Items</h3>
            </div>
            <div class="col-md-7 align-self-center">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                    <li class="breadcrumb-item">Item Details</li>
                    <li class="breadcrumb-item active">Add items</li>
                </ol>
            </div>
            <div class="">
                <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card card-outline-info">
                        <div class="card-header">
                            <h4 class="m-b-0 text-white">Add Items</h4>
                        </div>
                        <div class="card-body">
                            <form action="{{route('additem')}}" class="form-horizontal" method="post">
                                @csrf
                                <div class="form-body">

                                    <hr class="m-t-0 m-b-40">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group row">
                                                <label class="control-label text-right col-md-3">Item Name</label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" placeholder="" name="item_name" value="">
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                    </div>
                                    <!--/row-->
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group row">
                                                <label class="control-label text-right col-md-3">Item_qty</label>
                                                <div class="col-md-9">
                                                    <input type="number" class="form-control" placeholder="" name="item_qty" value="">
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                        <div class="col-md-6">
                                            <div class="form-group row">
                                                <label class="control-label text-right col-md-3">Item_rate</label>
                                                <div class="col-md-9">
                                                    <input type="number" class="form-control" placeholder="" name="item_rate" value="">
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                    </div>
                                    <!--/row-->
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group row">
                                                <label class="control-label text-right col-md-3">Item SerialNo</label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" placeholder="" name="item_serialNo" value="">
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                        <div class="col-md-6">
                                            <div class="form-group row">
                                                <label class="control-label text-right col-md-3">Item InvoiceNo</label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" placeholder="" name="item_invoiceNo" value="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group row">
                                                <label class="control-label text-right col-md-3">Item_cgst</label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" placeholder="" name="item_cgst" value="">
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                        <div class="col-md-6">
                                            <div class="form-group row">
                                                <label class="control-label text-right col-md-3">Item_sgst</label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" placeholder="" name="item_sgst" value="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group row">
                                                <label class="control-label text-right col-md-3">Item_gst</label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" placeholder="" name="item_gst" value="">
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                        <div class="col-md-6">
                                            <div class="form-group row">
                                                <label class="control-label text-right col-md-3">Item_igst</label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" placeholder="" name="item_igst" value="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group row">
                                                <label class="control-label text-right col-md-3">Total Price</label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" placeholder="" name="total_price" value="">
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                        <div class="col-md-6">
                                            <div class="form-group row">
                                                <label class="control-label text-right col-md-3">Item_isi</label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" placeholder="" name="item_isi" value="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group row">
                                                <label class="control-label text-right col-md-3">Item_tax</label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" placeholder="" name="item_tax" value="">
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                        <div class="col-md-6">
                                            <div class="form-group row">
                                                <label class="control-label text-right col-md-3">Item_hsn</label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" placeholder="" name="item_hsn" value="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group row">
                                                <label class="control-label text-right col-md-3">Item_suc</label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" placeholder="" name="item_suc" value="">
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                        <div class="col-md-6">
                                            <div class="form-group row">
                                                <label class="control-label text-right col-md-3">Item Unit</label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" placeholder="" name="item_unit" value="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group row">
                                                <label class="control-label text-right col-md-3">Item Specification</label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" placeholder="" name="item_spec" value="">
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                        <div class="col-md-6">
                                            <div class="form-group row">
                                                <label class="control-label text-right col-md-3">Item Description</label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" placeholder="" name="item_desc" value="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="row">
                                                    <div class="col-md-offset-3 col-md-9">
                                                        <button type="submit" class="btn btn-success">Submit</button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6"> </div>
                                        </div>
                                    </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop