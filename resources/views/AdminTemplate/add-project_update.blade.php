@extends('AdminTemplate.base')
@section('content')
    <div class="page-wrapper">
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h3 class="text-themecolor">Add-Project-Update</h3>
            </div>
            <div class="col-md-7 align-self-center">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                    <li class="breadcrumb-item">Project Update</li>
                    <li class="breadcrumb-item active">Add Project Update</li>
                </ol>
            </div>
            <div class="">
                <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card card-outline-info">
                        <div class="card-header">
                            <h4 class="m-b-0 text-white">Add Project Update</h4>
                        </div>
                        <div class="card-body">
                            <form action="{{route('addprojectupdate')}}" class="form-horizontal" method="post" enctype="multipart/form-data">
                                @csrf
                                <div class="form-body">
                                    <hr class="m-t-0 m-b-40">
                                    <div class="row">
                                        <div class="col-md-8">
                                            <div class="form-group row">
                                                <label class="control-label text-right col-md-3">Project Video Url:</label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" placeholder="" name="project_video_url" value="">
                                                </div>
                                            </div>
                                        </div>
{{--                                        <div class="col-md-6">--}}
{{--                                            <div class="form-group row">--}}
{{--                                                <label class="control-label text-right col-md-3">Customer Last Name</label>--}}
{{--                                                <div class="col-md-9">--}}
{{--                                                    <input type="text" class="form-control" placeholder="" name="customer_last_name" value="">--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
                                        <!--/span-->
{{--                                        <div class="col-md-6" style="display: none;">--}}
{{--                                            <div class="form-group row">--}}
{{--                                                <label class="control-label text-right col-md-3">Admin_Id</label>--}}
{{--                                                <div class="col-md-9">--}}
{{--                                                    <input type="text" class="form-control" placeholder="" name="admin_id" value="{{ Auth::user()->admin_id}}">--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
                                        <!--/span-->
                                    </div>
                                    <!--/row-->
                                    <div class="row">
                                        <div class="col-md-8">
                                            <div class="form-group row">
                                                <label class="control-label text-right col-md-3">Project Video Title:</label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" placeholder="" name="project_video_title" value="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="customer's_e-mail_id" class="col-md-4 col-form-label">Project Video Description:</label>
                                        <div class="col-md-10">
                                            <div class="form-group green-border-focus">
                                                <textarea class="form-control" name="project_video_description" rows="3" required></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="row">
                                                    <div class="col-md-offset-3 col-md-9">
                                                        <button type="submit" class="btn btn-success">Submit</button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6"> </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop