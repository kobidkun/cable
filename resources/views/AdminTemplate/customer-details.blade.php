@extends('AdminTemplate.base')
@section('content')
    <div class="page-wrapper">
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h3 class="text-themecolor">Customer Details</h3>
            </div>
            <div class="col-md-7 align-self-center">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                    <li class="breadcrumb-item">Customer</li>
                    <li class="breadcrumb-item active">Customer Details</li>
                </ol>
            </div>
            <div class="">
                <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card card-outline-info">
                        <div class="card-header">
                            <h4 class="m-b-0 text-white">Customer Details </h4>
                        </div>
                        <div class="card-body">
                            <form action="{{route('updatecustomer')}}" class="form-horizontal" method="post" enctype="multipart/form-data">
                                @csrf
                                <div class="form-body">
                                    <hr class="m-t-0 m-b-40">
                                    <input type="hidden" name="customer_id" value="{{$editcustomer->customer_id}}">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group row">
                                                <label class="control-label text-right col-md-3">Customer First Name</label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" placeholder="" name="customer_name" value="{{$editcustomer->customer_name}}">
                                                </div>
                                            </div>
                                        </div>
{{--                                        <div class="col-md-6">--}}
{{--                                            <div class="form-group row">--}}
{{--                                                <label class="control-label text-right col-md-3">Customer Last Name</label>--}}
{{--                                                <div class="col-md-9">--}}
{{--                                                    <input type="text" class="form-control" placeholder="" name="customer_last_name" value="{{$editcustomer->customer_last_name}}">--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
                                        <!--/span-->
                                        <div class="col-md-6" style="display: none;">
                                            <div class="form-group row">
                                                <label class="control-label text-right col-md-3">Admin_Id</label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" placeholder="" name="admin_id" value="{{$editcustomer->admin_id}}">
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                    </div>
                                    <!--/row-->
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group row">
                                                <label class="control-label text-right col-md-3">Email</label>
                                                <div class="col-md-9">
                                                    <input type="email" class="form-control" placeholder="" name="customer_email" value="{{$editcustomer->customer_email}}">
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                        <div class="col-md-6">
                                            <div class="form-group row">
                                                <label class="control-label text-right col-md-3">Phone</label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" placeholder="" name="customer_phone" value="{{$editcustomer->customer_phone}}">
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                    </div>
                                    <!--/row-->
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group row">
                                                <label class="control-label text-right col-md-3">Address</label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" placeholder="" name="customer_addr" value="{{$editcustomer->customer_addr}}">
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                        <div class="col-md-6">
                                            <div class="form-group row">
                                                <label class="control-label text-right col-md-3">Customer Image</label>
                                                <div class="col-md-9">
                                                    <input type="file" accept="image/*" class="form-control" placeholder="" name="customer_img" value="{{$editcustomer->customer_img}}">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group row">
                                                <label class="control-label text-right col-md-3">Customer Latitude</label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" placeholder="" name="customerAddr_lati" value="{{$editcustomer->customerAddr_lati}}">
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                        <div class="col-md-6">
                                            <div class="form-group row">
                                                <label class="control-label text-right col-md-3">Customer Longitude</label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" placeholder="" name="customerAddr_long" value="{{$editcustomer->customerAddr_long}}">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="row">
                                                    <div class="col-md-offset-3 col-md-9">
                                                        <button type="submit" class="btn btn-success">Edit</button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6"> </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop