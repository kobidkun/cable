@extends('AdminTemplate.base')
@section('content')
    <div class="page-wrapper">
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h3 class="text-themecolor">Event-Image</h3>
            </div>
            <div class="col-md-7 align-self-center">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                    <li class="breadcrumb-item">Event Image</li>
                    <li class="breadcrumb-item active">Event Image</li>
                </ol>
            </div>
            <div class="">
                <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card card-outline-info">
                        <div class="card-header">
                            <h4 class="m-b-0 text-white">Event Image</h4>
                        </div>
                        <div class="card-body">
                            <form action="{{route('addeventimage')}}" class="form-horizontal" method="post" enctype="multipart/form-data">
                                @csrf
                                <div class="form-body">
                                    <hr class="m-t-0 m-b-40">
                                    <div class="row">
                                        <div class="col-md-8">
                                            <div class="form-group row">
                                                <label class="control-label text-right col-md-3">Event Name:</label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" placeholder="Enter ..." name="event_name" value="" id="event-name">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="control-label text-right col-md-3">Event id:</label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" placeholder="" name="event_id" value="" id="event_id">
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <!--/row-->
                                    <div class="row">
                                        <div class="col-md-8">
                                            <div class="form-group row">
                                                <label class="control-label text-right col-md-3">event_big_image_url:</label>
                                                <div class="col-md-9">
                                                    <input type="file" accept="image/*" class="form-control" placeholder="" name="event_big_image_url">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="form-group row">
                                                <label class="control-label text-right col-md-3">event_small_image_url:</label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" placeholder="" name="event_small_image_url" value="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="row">
                                                    <div class="col-md-offset-3 col-md-9">
                                                        <button type="submit" class="btn btn-success">Update</button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6"> </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop

@section('scripts')
@endsection