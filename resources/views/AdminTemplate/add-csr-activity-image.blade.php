@extends('AdminTemplate.base')
@section('content')
    <div class="page-wrapper">
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h3 class="text-themecolor">CSR-Activity-Image</h3>
            </div>
            <div class="col-md-7 align-self-center">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                    <li class="breadcrumb-item">CSR Activity Image</li>
                    <li class="breadcrumb-item active">CSR Activity Image</li>
                </ol>
            </div>
            <div class="">
                <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card card-outline-info">
                        <div class="card-header">
                            <h4 class="m-b-0 text-white">CSR Activity Image</h4>
                        </div>
                        <div class="card-body">
                            <form action="{{route('addcsrimage')}}" class="form-horizontal" method="post" enctype="multipart/form-data">
                                @csrf
                                <div class="form-body">
                                    <hr class="m-t-0 m-b-40">
                                    <input type="hidden" name="csr_activity_id" value="">
                                    <div class="row">
                                        <div class="col-md-8">
                                            <div class="form-group row">
                                                <label class="control-label text-right col-md-3">csr_activity_id:</label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" placeholder="" name="csr_activity_id" value="" id="csr_activity_id">
                                                </div>
{{--                                                <div id="csr_activity_to_images_id">--}}
{{--                                                </div>--}}
                                            </div>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="form-group row">
                                                <label class="control-label text-right col-md-3">csr_activity_title:</label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" placeholder="" name="csr_activity_title" value="" id="csr_activity_title">
                                                </div>
                                                {{--                                                <div id="csr_activity_to_images_id">--}}
                                                {{--                                                </div>--}}
                                            </div>
                                        </div>
                                    </div>
                                    <!--/row-->
                                    <div class="row">
                                        <div class="col-md-8">
                                            <div class="form-group row">
                                                <label class="control-label text-right col-md-3">csr_activity_big_image_url:</label>
                                                <div class="col-md-9">
                                                    <input type="file" class="form-control" placeholder="" name="csr_activity_big_image_url" value="">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="form-group row">
                                                <label class="control-label text-right col-md-3">csr_activity_small_image_url:</label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" placeholder="" name="csr_activity_small_image_url" value="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="row">
                                                    <div class="col-md-offset-3 col-md-9">
                                                        <button type="submit" class="btn btn-success">Update</button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6"> </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop

{{--@section('scripts')--}}
{{--    <script>--}}
{{--        $(document).ready(function(){--}}

{{--            $('#csr_activity_id').keyup(function(){--}}
{{--                var query = $(this).val();--}}
{{--                if(query != '')--}}
{{--                {--}}
{{--                    var _token = $('input[name="_token"]').val();--}}
{{--                    $.ajax({--}}
{{--                        url:"{{ route('autocomplete.fetch') }}",--}}
{{--                        method:"POST",--}}
{{--                        data:{query:query, _token:_token},--}}
{{--                        success:function(data){--}}
{{--                            $('#csr_activity_to_images_id').fadeIn();--}}
{{--                            $('#csr_activity_to_images_id').html(data);--}}
{{--                        }--}}
{{--                    });--}}
{{--                }--}}
{{--            });--}}

{{--            $(document).on('click', 'li', function(){--}}
{{--                $('#csr_activity_title').val($(this).text());--}}
{{--                $('#csr_activity_to_images_id').fadeOut();--}}
{{--            });--}}

{{--        });--}}
{{--    </script>--}}
{{--@endsection--}}