@extends('AdminTemplate.base')
@section('content')
    <div class="page-wrapper">
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h3 class="text-themecolor">Contact-Details</h3>
            </div>
            <div class="col-md-7 align-self-center">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                    <li class="breadcrumb-item">Contacts</li>
                    <li class="breadcrumb-item active">Contact Details</li>
                </ol>
            </div>
            <div class="">
                <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card card-outline-info">
                        <div class="card-header">
                            <h4 class="m-b-0 text-white"> Contact Details</h4>
                        </div>
                        <div class="card-body">
                            <form action="{{url('/contact')}}" class="form-horizontal" >
                                @csrf
                                <div class="form-body">
                                    <hr class="m-t-0 m-b-40">
                                    <input type="hidden" name="user_id" value="{{$editcontact->user_id}}">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group row">
                                                <label class="control-label text-right col-md-3">user_name</label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" placeholder="" name="user_name" value="{{$editcontact->user_name }}">
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                        <div class="col-md-6">
                                            <div class="form-group  row">
                                                <label class="control-label text-right col-md-3">user_phone</label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" placeholder="" name="user_phone" value="{{$editcontact-> user_phone}}">
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                    </div>
                                    <!--/row-->
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group row">
                                                <label class="control-label text-right col-md-3">user_email </label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" placeholder="" name="user_email" value="{{$editcontact->user_email }}">
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                        <div class="col-md-6">
                                            <div class="form-group row">
                                                <label class="control-label text-right col-md-3">user_question</label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" placeholder="" name="user_question" value="{{$editcontact->user_question }}">
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                    </div>
                                    <!--/row-->
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group row">
                                                <label class="control-label text-right col-md-3">tmp1</label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" placeholder="" name="tmp1" value="{{$editcontact->tmp1 }}">
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                        <div class="col-md-6">
                                            <div class="form-group row">
                                                <label class="control-label text-right col-md-3">tmp2</label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" placeholder="" name="tmp2" value="{{$editcontact->tmp2 }}">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group row">
                                                <label class="control-label text-right col-md-3">tmp3</label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" placeholder="" name="tmp3" value="{{$editcontact->tmp3 }}">
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                        <div class="col-md-6">
                                            <div class="form-group row">
                                                <label class="control-label text-right col-md-3">tmp4</label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" placeholder="" name="tmp4" value="{{$editcontact->tmp4 }}">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="row">
                                                    <div class="col-md-offset-3 col-md-9">
                                                        <button type="submit" class="btn btn-success">OK</button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6"> </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@stop