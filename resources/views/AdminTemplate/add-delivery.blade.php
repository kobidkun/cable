@extends('AdminTemplate.base')
@section('content')
    <div class="page-wrapper">
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h3 class="text-themecolor">Add-Delivery</h3>
            </div>
            <div class="col-md-7 align-self-center">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                    <li class="breadcrumb-item">Delivery</li>
                    <li class="breadcrumb-item active">Add Delivery</li>
                </ol>
            </div>
            <div class="">
                <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card card-outline-info">
                        <div class="card-header">
                            <h4 class="m-b-0 text-white">Add Delivery</h4>
                        </div>
                        <div class="card-body">
                            <form action="{{route('adddeliver')}}" class="form-horizontal" method="post">
                                @csrf
                                <div class="form-body">
                                    <hr class="m-t-0 m-b-40">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group row">
                                                <label class="control-label text-right col-md-3">delivery_status</label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" placeholder="" name="delivery_status" value="">
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                        <div class="col-md-6">
                                            <div class="form-group  row">
                                                <label class="control-label text-right col-md-3">Order Id</label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" placeholder="" name="order_id" value="">
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                    </div>
                                    <!--/row-->
                                    <div class="row">

                                        <!--/span-->
                                        <div class="col-md-6">
                                            <div class="form-group row">
                                                <label class="control-label text-right col-md-3">Salesmanid</label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" placeholder="" name="salesman_id" value="">
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
{{--                                        <div class="col-md-6">--}}
{{--                                            <div class="form-group row">--}}
{{--                                                <label class="control-label text-right col-md-3">Delivery Location</label>--}}
{{--                                                <div class="col-md-9">--}}
{{--                                                    <input type="text" class="form-control" placeholder="" name="delivery_location" value="">--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
                                        <div class="col-md-6">
                                            <div class="form-group row">
                                                <label class="control-label text-right col-md-3">amount_paid</label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" placeholder="" name="amount_paid" value="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/row-->
                                    <hr>
                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="row">
                                                    <div class="col-md-offset-3 col-md-9">
                                                        <button type="submit" class="btn btn-success">Submit</button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6"> </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@stop