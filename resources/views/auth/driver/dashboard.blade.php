@extends('auth.driver.layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading"><strong>Driver's</strong> Dashboard</div>

                    <div class="panel-body">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif

                        <p>Welcome Mr./Mst : <strong>{{ Auth::user()->driver_name}}</strong></p>
                        <p>Your joined  : {{ Auth::user()->created_at->diffForHumans() }} </p>
                            @if (!Auth::user()->email_verified_at)
                                <form method="post" action="{{route('admin.sendmail')}}">
                                    @csrf
                                    <input type="hidden" name="guard_type" value="admin">
                                    <input type="hidden" name="admin_name" value="{{Auth::user()->admin_name}}">
                                    <input type="hidden" name="admin_id" value="{{Auth::user()->admin_id}}">
                                    <input type="hidden" name="admin_email" value="{{Auth::user()->admin_email}}">
                                    <button type="submit">Verify your Email Address</button>
                                </form>
                            @else
                                You have already verified your email address
                            @endif
                            @if (!Auth::user()->phone_verified_at)
                                <form method="post" action="{{route('admin.sendotp')}}">
                                    @csrf
                                    <input type="hidden" name="admin_name" value="{{Auth::user()->admin_name}}">
                                    <input type="hidden" name="admin_id" value="{{Auth::user()->admin_id}}">
                                    <input type="hidden" name="admin_phone" value="{{Auth::user()->admin_phone}}">
                                    <button type="submit">Verify your Phone</button>
                                </form>
                            @else
                                You have already verified your Phone
                            @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection