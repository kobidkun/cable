@extends('auth.driver.layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Driver Register</div>

                    <div class="panel-body">
                        <form role="form" id="form-register" method="post" action="{{route('driver.register.store')}}" enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" name="car_id" value="{{Uuid::generate()->string}}">
                            <input type="hidden" name="driver_uuid" value="{{Uuid::generate()->string}}">
                            <div class="form-group {{ $errors->has('driver_name') ? ' has-error' : '' }}">
                                <label for="driver_name">First Name:</label>
                                <input type="text" class="form-control" id="driver_name" name="driver_name" required>

                                @if ($errors->has('driver_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('driver_name') }}</strong>
                                    </span>
                                @endif
                            </div><!-- /.form-group -->
{{--                            <div class="form-group {{ $errors->has('driver_last_name') ? ' has-error' : '' }}">--}}
{{--                                <label for="driver_name">Last Name:</label>--}}
{{--                                <input type="text" class="form-control" id="driver_last_name" name="driver_last_name" required>--}}

{{--                                @if ($errors->has('driver_last_name'))--}}
{{--                                    <span class="help-block">--}}
{{--                                        <strong>{{ $errors->first('driver_last_name') }}</strong>--}}
{{--                                    </span>--}}
{{--                                @endif--}}
{{--                            </div><!-- /.form-group -->--}}
{{--                            <div class="form-group {{ $errors->has('city_id') ? ' has-error' : '' }}">--}}
{{--                                <label for="city_id">City ID:</label>--}}
{{--                                <input type="text" class="form-control" id="city_id" name="city_id" required>--}}

{{--                                @if ($errors->has('city_id'))--}}
{{--                                    <span class="help-block">--}}
{{--                                        <strong>{{ $errors->first('city_id') }}</strong>--}}
{{--                                    </span>--}}
{{--                                @endif--}}
{{--                            </div><!-- /.form-group -->--}}
                            <div class="form-group {{ $errors->has('driver_email') ? ' has-error' : '' }}">
                                <label for="driver_email">Email:</label>
                                <input type="email" class="form-control" id="driver_email" name="driver_email" required>

                                @if ($errors->has('driver_email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('driver_email') }}</strong>
                                    </span>
                                @endif
                            </div><!-- /.form-group -->
                            <div class="form-group{{ $errors->has('driver_phone') ? ' has-error' : '' }}">
                                <label for="driver_phone">Phone Number:</label>
                                <input type="number" class="form-control" id="driver_phone" name="driver_phone" required>

                                @if ($errors->has('driver_phone'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('driver_phone') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group{{ $errors->has('driver_license') ? ' has-error' : '' }}">
                                <label for="driver_license">License Number:</label>
                                <input type="text" class="form-control" id="driver_license" name="driver_license" required>

                                @if ($errors->has('driver_license'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('driver_license') }}</strong>
                                    </span>
                                @endif
                            </div><!-- /.form-group -->
                            <div class="form-group row">
                                <label for="driver_image" class="col-md-4 col-form-label text-md-right">Profile Image</label>
                                <div class="col-md-6">
                                    <input id="driver_image" type="file" class="form-control" name="driver_image">
{{--                                    @if (auth()->user()->image)--}}
{{--                                        <code>{{ auth()->user()->image }}</code>--}}
{{--                                    @endif--}}
                                </div>
                            </div>
                            <div class="form-group {{ $errors->has('driver_password') ? ' has-error' : '' }}">
                                <label for="driver_password">Password:</label>
                                <input type="password" class="form-control" id="driver_password" name="driver_password" required>

                                @if ($errors->has('driver_password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('driver_password') }}</strong>
                                    </span>
                                @endif
                            </div><!-- /.form-group -->
                            <div class="form-group">
                                <label for="form-register-confirm-password">Confirm Password:</label>
                                <input type="password" class="form-control" id="form-register-confirm-password" name="form-register-confirm-password" required>
                            </div><!-- /.form-group -->
                            <div class="form-group clearfix">
                                <button type="submit" class="btn pull-right btn-default" id="account-submit">Create an Account</button>
                                <a href="{{route('driver.auth.login')}}" class="btn pull-right btn-link" style="text-decoration: #c77405">Login</a>
                            </div><!-- /.form-group -->
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection