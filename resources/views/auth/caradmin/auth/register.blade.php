@extends('auth.caradmin.layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Car Admin Register</div>

                    <div class="panel-body">
                        <form role="form" id="form-register" method="post" action="{{route('caradmin.register.store')}}">
                            @csrf
{{--                            <input type="hidden" name="car_id" value="{{Uuid::generate()->string}}">--}}
                            <input type="hidden" name="car_admin_uuid" value="{{Uuid::generate()->string}}">
                            <div class="form-group {{ $errors->has('car_admin_name') ? ' has-error' : '' }}">
                                <label for="car_admin_name">First Name:</label>
                                <input type="text" class="form-control" id="car_admin_name" name="car_admin_name" required>

                                @if ($errors->has('car_admin_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('car_admin_name') }}</strong>
                                    </span>
                                @endif
                            </div><!-- /.form-group -->
{{--                            <div class="form-group {{ $errors->has('car_admin_last_name') ? ' has-error' : '' }}">--}}
{{--                                <label for="car_admin_name">Last Name:</label>--}}
{{--                                <input type="text" class="form-control" id="car_admin_last_name" name="car_admin_last_name" required>--}}

{{--                                @if ($errors->has('car_admin_last_name'))--}}
{{--                                    <span class="help-block">--}}
{{--                                        <strong>{{ $errors->first('car_admin_last_name') }}</strong>--}}
{{--                                    </span>--}}
{{--                                @endif--}}
{{--                            </div><!-- /.form-group -->--}}
{{--                            <div class="form-group {{ $errors->has('city_id') ? ' has-error' : '' }}">--}}
{{--                                <label for="city_id">City ID:</label>--}}
{{--                                <input type="text" class="form-control" id="city_id" name="city_id" required>--}}

{{--                                @if ($errors->has('city_id'))--}}
{{--                                    <span class="help-block">--}}
{{--                                        <strong>{{ $errors->first('city_id') }}</strong>--}}
{{--                                    </span>--}}
{{--                                @endif--}}
{{--                            </div><!-- /.form-group -->--}}
                            <div class="form-group {{ $errors->has('car_admin_email') ? ' has-error' : '' }}">
                                <label for="car_admin_email">Email:</label>
                                <input type="email" class="form-control" id="car_admin_email" name="car_admin_email" required>

                                @if ($errors->has('car_admin_email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('car_admin_email') }}</strong>
                                    </span>
                                @endif
                            </div><!-- /.form-group -->
                            <div class="form-group{{ $errors->has('car_admin_phone') ? ' has-error' : '' }}">
                                <label for="car_admin_phone">Phone Number:</label>
                                <input type="number" class="form-control" id="car_admin_phone" name="car_admin_phone" required>

                                @if ($errors->has('car_admin_phone'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('car_admin_phone') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group {{ $errors->has('car_admin_password') ? ' has-error' : '' }}">
                                <label for="car_admin_password">Password:</label>
                                <input type="password" class="form-control" id="car_admin_password" name="car_admin_password" required>

                                @if ($errors->has('car_admin_password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('car_admin_password') }}</strong>
                                    </span>
                                @endif
                            </div><!-- /.form-group -->
                            <div class="form-group">
                                <label for="form-register-confirm-password">Confirm Password:</label>
                                <input type="password" class="form-control" id="form-register-confirm-password" name="form-register-confirm-password" required>
                            </div><!-- /.form-group -->
                            <div class="form-group clearfix">
                                <button type="submit" class="btn pull-right btn-default" id="account-submit">Create an Account</button>
                                <a href="{{route('caradmin.auth.login')}}" class="btn pull-right btn-link" style="text-decoration: #c77405">Login</a>
                            </div><!-- /.form-group -->
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection