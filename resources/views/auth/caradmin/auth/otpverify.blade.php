@extends('auth.caradmin.layouts.app')

@section('title', 'login here')


@section('content')
<div class="container">
<form action="{{route('caradmin.otpverify')}}" method="post">
    @csrf
    <input type="hidden" name="car_admin_phone" value="{{auth()->user()->car_admin_phone}}">
    <div class="form-group">
        <label for="otpEntered">Enter OTP:</label>
        <input type="text" class="form-control" id="otpEntered" name="otpEntered">
        <button type="submit">Submit</button>
        <form method="post">
            <input type="hidden" name="car_admin_phone" value="{{auth()->user()->car_admin_phone}}">
            <button type="button" onclick="location.href='{{route('caradmin.otpform')}}'">resend</button>
        </form>
    </div>
</form>
</div>
@endsection