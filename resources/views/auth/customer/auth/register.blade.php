@extends('auth.customer.layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Customer Register</div>

                    <div class="panel-body">
                        <form role="form" id="form-register" method="post" action="{{route('customer.register.store')}}">
                            @csrf
                            <input type="hidden" name="customer_uuid" value="{{Uuid::generate()->string}}">
                            <div class="form-group {{ $errors->has('customer_name') ? ' has-error' : '' }}">
                                <label for="customer_name">First Name:</label>
                                <input type="text" class="form-control" id="customer_name" name="customer_name" required>

                                @if ($errors->has('customer_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('customer_name') }}</strong>
                                    </span>
                                @endif
                            </div><!-- /.form-group -->
{{--                            <div class="form-group {{ $errors->has('customer_last_name') ? ' has-error' : '' }}">--}}
{{--                                <label for="customer_name">Last Name:</label>--}}
{{--                                <input type="text" class="form-control" id="customer_last_name" name="customer_last_name" required>--}}

{{--                                @if ($errors->has('customer_last_name'))--}}
{{--                                    <span class="help-block">--}}
{{--                                        <strong>{{ $errors->first('customer_last_name') }}</strong>--}}
{{--                                    </span>--}}
{{--                                @endif--}}
{{--                            </div><!-- /.form-group -->--}}
{{--                            <div class="form-group {{ $errors->has('city_id') ? ' has-error' : '' }}">--}}
{{--                                <label for="city_id">City ID:</label>--}}
{{--                                <input type="text" class="form-control" id="city_id" name="city_id" required>--}}

{{--                                @if ($errors->has('city_id'))--}}
{{--                                    <span class="help-block">--}}
{{--                                        <strong>{{ $errors->first('city_id') }}</strong>--}}
{{--                                    </span>--}}
{{--                                @endif--}}
{{--                            </div><!-- /.form-group -->--}}
                            <div class="form-group {{ $errors->has('customer_email') ? ' has-error' : '' }}">
                                <label for="customer_email">Email:</label>
                                <input type="email" class="form-control" id="customer_email" name="customer_email" required>

                                @if ($errors->has('customer_email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('customer_email') }}</strong>
                                    </span>
                                @endif
                            </div><!-- /.form-group -->
                            <div class="form-group{{ $errors->has('customer_phone') ? ' has-error' : '' }}">
                                <label for="customer_phone">Phone Number:</label>
                                <input type="number" class="form-control" id="customer_phone" name="customer_phone" required>

                                @if ($errors->has('customer_phone'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('customer_phone') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group{{ $errors->has('customer_license') ? ' has-error' : '' }}">
                                <label for="customer_license">License Number:</label>
                                <input type="text" class="form-control" id="customer_license" name="customer_license">

                                @if ($errors->has('customer_license'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('customer_license') }}</strong>
                                    </span>
                                @endif
                            </div><!-- /.form-group -->
                            <div class="form-group{{ $errors->has('customer_city') ? ' has-error' : '' }}">
                                <label for="customer_city">Customer City:</label>
                                <input type="text" class="form-control" id="customer_city" name="customer_city">

                                @if ($errors->has('customer_city'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('customer_city') }}</strong>
                                    </span>
                                @endif
                            </div><!-- /.form-group -->
                            <div class="form-group{{ $errors->has('customer_lat') ? ' has-error' : '' }}">
                                <label for="customer_lat">Customer Latitude:</label>
                                <input type="text" class="form-control" id="customer_lat" name="customer_lat">

                                @if ($errors->has('customer_lat'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('customer_lat') }}</strong>
                                    </span>
                                @endif
                            </div><!-- /.form-group -->
                            <div class="form-group{{ $errors->has('customer_long') ? ' has-error' : '' }}">
                                <label for="customer_long">Customer Longitude:</label>
                                <input type="text" class="form-control" id="customer_long" name="customer_long">

                                @if ($errors->has('customer_long'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('customer_long') }}</strong>
                                    </span>
                                @endif
                            </div><!-- /.form-group -->
                            <div class="form-group{{ $errors->has('customer_altitude') ? ' has-error' : '' }}">
                                <label for="customer_altitude">Customer Altitude:</label>
                                <input type="text" class="form-control" id="customer_altitude" name="customer_altitude">

                                @if ($errors->has('customer_altitude'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('customer_altitude') }}</strong>
                                    </span>
                                @endif
                            </div><!-- /.form-group -->
                            <div class="form-group {{ $errors->has('customer_password') ? ' has-error' : '' }}">
                                <label for="customer_password">Password:</label>
                                <input type="password" class="form-control" id="customer_password" name="customer_password" required>

                                @if ($errors->has('customer_password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('customer_password') }}</strong>
                                    </span>
                                @endif
                            </div><!-- /.form-group -->
                            <div class="form-group">
                                <label for="form-register-confirm-password">Confirm Password:</label>
                                <input type="password" class="form-control" id="form-register-confirm-password" name="form-register-confirm-password" required>
                            </div><!-- /.form-group -->
                            <div class="form-group clearfix">
                                <button type="submit" class="btn pull-right btn-default" id="account-submit">Create an Account</button>
                                <a href="{{route('customer.auth.login')}}" class="btn pull-right btn-link" style="text-decoration: #c77405">Login</a>
                            </div><!-- /.form-group -->
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
