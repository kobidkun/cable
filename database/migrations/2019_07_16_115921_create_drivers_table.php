<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDriversTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('drivers', function (Blueprint $table) {
            $table->text('id');
            $table->timestamp('email_verified_at')->nullable();
            $table->timestamp('phone_verified_at')->nullable();
            $table->text('verifier')->nullable();
            $table->text('otp_verifier')->nullable();
            $table->text('driver_name');
            $table->text('driver_phone');
            $table->text('driver_email');
            $table->text('driver_license');
            $table->text('driver_password');
            $table->text('car_id');
            $table->text('driver_image')->nullable();
            $table->text('driver_rating')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('drivers');
    }
}
