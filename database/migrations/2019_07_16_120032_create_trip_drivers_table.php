<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTripDriversTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trip_drivers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('car_id');
            $table->text('customer_uuid');
            $table->text('driver_uuid');
            $table->float('pickup_lat');
            $table->float('pickup_long');
            $table->text('pickup_address');
            $table->float('drop_lat');
            $table->float('drop_long');
            $table->text('drop_address');
            $table->float('total_distance');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trip_drivers');
    }
}
