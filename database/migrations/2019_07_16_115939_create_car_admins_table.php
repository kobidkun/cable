<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarAdminsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('car_admins', function (Blueprint $table) {
            $table->text('id');
            $table->timestamp('email_verified_at')->nullable();
            $table->timestamp('phone_verified_at')->nullable();
            $table->text('verifier')->nullable();
            $table->text('otp_verifier')->nullable();
            $table->text('car_admin_name');
            $table->text('car_admin_email');
            $table->text('car_admin_phone');
            $table->text('car_admin_password');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('car_admins');
    }
}
