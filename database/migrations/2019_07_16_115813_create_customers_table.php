<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->float('customer_lat')->nullable();
            $table->float('customer_long')->nullable();
            $table->float('customer_altitude')->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->timestamp('phone_verified_at')->nullable();
            $table->text('verifier')->nullable();
            $table->text('otp_verifier')->nullable();
            $table->text('customer_name')->nullable();
            $table->text('customer_city')->nullable();
            $table->text('customer_phone');
            $table->text('customer_email')->nullable();
            $table->text('customer_password')->nullable();
            $table->text('customer_license')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}
