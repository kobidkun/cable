<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTripRatingSolosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trip_rating_solos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('trip_solo_id');
            $table->integer('rating_number');
            $table->text('rating_title');
            $table->text('rating_description');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trip_rating_solos');
    }
}
