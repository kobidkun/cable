<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentSolosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_solos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('trip_solo_id');
            $table->float('total_price');
            $table->integer('tax');
            $table->integer('cgst');
            $table->integer('sgst');
            $table->text('payment_method');
            $table->text('card_no')->nullable();
            $table->text('gateway_transaction_id')->nullable();
            $table->time('gateway_create_time')->nullable();
            $table->time('gateway_update_time')->nullable();
            $table->time('gateway_state')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_solos');
    }
}
