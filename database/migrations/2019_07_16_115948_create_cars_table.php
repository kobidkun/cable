<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cars', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('car_number');
            $table->text('car_city');
            $table->float('car_lat');
            $table->float('car_long');
            $table->float('car_altitude');
            $table->boolean('is_moving');
            $table->text('car_admin_uuid');
            $table->text('car_type');
            $table->text('car_model');
            $table->text('car_fuel_type');
            $table->text('car_capacity');
            $table->text('car_image')->nullable();
            $table->text('driver_uuid')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cars');
    }
}
