<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Route::get('profile', function () {
    return "This is a Verified Only Page";
})->middleware('verified');
Route::get('/home', 'HomeController@index')->name('home');

Route::prefix('admin')->group(function () {
    Route::get('profile', function () {
        return "This is a Verified Only Page";
    });
    Route::get('/', 'Auth\Admin\AdminController@index')->name('admin.dashboard');
    Route::get('dashboard', 'Auth\Admin\AdminController@index')->name('admin.dashboard');
    Route::get('register', 'Auth\Admin\AdminController@create')->name('admin.register');
    Route::post('register', 'Auth\Admin\AdminController@store')->name('admin.register.store');
    Route::get('login', 'Auth\Admin\LoginController@login')->name('admin.auth.login');
    Route::post('login', 'Auth\Admin\LoginController@loginAdmin')->name('admin.auth.loginAdmin');
    Route::post('logout', 'Auth\Admin\LoginController@logout')->name('admin.auth.logout');
    Route::post('/sendotp', 'Auth\Admin\AdminController@sendSMS')->name('admin.sendotp');
    Route::get('/otpform', 'Auth\Admin\AdminController@otpform')->name('admin.otpform');
    Route::post('/otpverify', 'Auth\Admin\AdminController@otp')->name('admin.otpverify');
    Route::post('/email-verify', 'Auth\Admin\MailController@authentication')->name('admin.sendmail');
    Route::get('/email-resend', 'Auth\Admin\MailController@emailresend')->name('admin.emailresend');
    Route::get('verifier/{verifier}/{email}', 'Auth\Admin\AdminController@verifier')->name('admin.emailverify');
    Route::get('/emailverified', 'HomeController@verifiedpage')->name('admin.verified.page');
    Route::get('/email-not-verified', 'HomeController@notverifiedpage')->name('admin.notverified.page');
    Route::get('/addcar', 'Auth\Admin\AdminController@addCarPage')->name('admin.addcarpage');
    Route::post('/addcar', 'Auth\Admin\AdminController@addcar')->name('admin.addcar.post');
    Route::get('/getcars', 'Auth\Admin\AdminController@getcars')->name('admin.getcars');
    Route::get('/addcaradmin', 'Auth\Admin\AdminController@addCarAdminPage')->name('admin.addCarAdminPage');
    Route::post('/addcaradmin', 'Auth\Admin\AdminController@addCarAdmin')->name('admin.addcaradmin.post');
    Route::get('/getcaradmins', 'Auth\Admin\AdminController@getcaradmins')->name('admin.getcaradmins');
    Route::get('/addcity', 'Auth\Admin\AdminController@addCityPage')->name('admin.addCityPage');
    Route::post('/addcity', 'Auth\Admin\AdminController@addCity')->name('admin.addcity.post');
    Route::get('/getcities', 'Auth\Admin\AdminController@getcities')->name('admin.getcities');
    Route::get('/addcartype', 'Auth\Admin\AdminController@addCarTypePage')->name('admin.addCarTypePage');
    Route::post('/addcartype', 'Auth\Admin\AdminController@addCarType')->name('admin.addcartype.post');
    Route::get('/getcartypes', 'Auth\Admin\AdminController@getcartypes')->name('admin.getcartypes');
    Route::get('/addtripdriver', 'Auth\Admin\AdminController@addTripDriverPage')->name('admin.addTripDriverPage');
    Route::post('/addtripdriver', 'Auth\Admin\AdminController@addTripDriver')->name('admin.adddriver.post');
    Route::get('/gettripdrivers', 'Auth\Admin\AdminController@getTripDrivers')->name('admin.getTripDrivers');
    Route::get('/adddriver', 'Auth\Admin\AdminController@addDriverPage')->name('admin.addDriverPage');
    Route::post('/adddriver', 'Auth\Admin\AdminController@addDriver')->name('admin.adddriver.post');
    Route::get('/getdrivers', 'Auth\Admin\AdminController@getDrivers')->name('admin.getdrivers');
});

Route::prefix('customer')->group(function () {
    Route::get('/', 'Auth\Customer\CustomerController@index')->name('customer.dashboard');
    Route::get('dashboard', 'Auth\Customer\CustomerController@index')->name('customer.dashboard');
    Route::get('register', 'Auth\Customer\CustomerController@create')->name('customer.register');
    Route::post('register', 'Auth\Customer\CustomerController@store')->name('customer.register.store');
    Route::get('login', 'Auth\Customer\LoginController@login')->name('customer.auth.login');
    Route::post('login', 'Auth\Customer\LoginController@loginCustomer')->name('customer.auth.loginCustomer');
    Route::post('logout', 'Auth\Customer\LoginController@logout')->name('customer.auth.logout');
    Route::post('/sendotp', 'Auth\Customer\CustomerController@sendSMS')->name('customer.sendotp');
    Route::get('/otpform', 'Auth\Customer\CustomerController@otpform')->name('customer.otpform');
    Route::post('/otpverify', 'Auth\Customer\CustomerController@otp')->name('customer.otpverify');
    Route::post('/email-verify', 'Auth\Customer\MailController@authentication')->name('customer.sendmail');
    Route::get('/email-resend', 'Auth\Customer\MailController@emailresend')->name('customer.emailresend');
    Route::get('verifier/{verifier}/{email}', 'Auth\Customer\CustomerController@verifier')->name('customer.emailverify');
    Route::get('/emailverified', 'HomeController@verifiedpage')->name('customer.verified.page');
    Route::get('/email-not-verified', 'HomeController@notverifiedpage')->name('customer.notverified.page');
});
Route::prefix('driver')->group(function () {
    Route::get('/', 'Auth\Driver\DriverController@index')->name('driver.dashboard');
    Route::get('dashboard', 'Auth\Driver\DriverController@index')->name('driver.dashboard');
    Route::get('register', 'Auth\Driver\DriverController@create')->name('driver.register');
    Route::post('register', 'Auth\Driver\DriverController@store')->name('driver.register.store');
    Route::get('login', 'Auth\Driver\LoginController@login')->name('driver.auth.login');
    Route::post('login', 'Auth\Driver\LoginController@loginDriver')->name('driver.auth.loginDriver');
    Route::post('logout', 'Auth\Driver\LoginController@logout')->name('driver.auth.logout');
    Route::post('/sendotp', 'Auth\Driver\DriverController@sendSMS')->name('driver.sendotp');
    Route::get('/otpform', 'Auth\Driver\DriverController@otpform')->name('driver.otpform');
    Route::post('/otpverify', 'Auth\Driver\DriverController@otp')->name('driver.otpverify');
    Route::post('/email-verify', 'Auth\Driver\MailController@authentication')->name('driver.sendmail');
    Route::get('/email-resend', 'Auth\Driver\MailController@emailresend')->name('driver.emailresend');
    Route::get('verifier/{verifier}/{email}', 'Auth\Driver\DriverController@verifier')->name('driver.emailverify');
    Route::get('/emailverified', 'HomeController@verifiedpage')->name('driver.verified.page');
    Route::get('/email-not-verified', 'HomeController@notverifiedpage')->name('driver.notverified.page');
});
Route::prefix('caradmin')->group(function () {
    Route::get('/', 'Auth\CarAdmin\CarAdminController@index')->name('caradmin.dashboard');
    Route::get('dashboard', 'Auth\CarAdmin\CarAdminController@index')->name('caradmin.dashboard');
    Route::get('register', 'Auth\CarAdmin\CarAdminController@create')->name('caradmin.register');
    Route::post('register', 'Auth\CarAdmin\CarAdminController@store')->name('caradmin.register.store');
    Route::get('login', 'Auth\CarAdmin\LoginController@login')->name('caradmin.auth.login');
    Route::post('login', 'Auth\CarAdmin\LoginController@loginCarAdmin')->name('caradmin.auth.loginCarAdmin');
    Route::post('logout', 'Auth\CarAdmin\LoginController@logout')->name('caradmin.auth.logout');
    Route::post('/sendotp', 'Auth\CarAdmin\CarAdminController@sendSMS')->name('caradmin.sendotp');
    Route::get('/otpform', 'Auth\CarAdmin\CarAdminController@otpform')->name('caradmin.otpform');
    Route::post('/otpverify', 'Auth\CarAdmin\CarAdminController@otp')->name('caradmin.otpverify');
    Route::post('/email-verify', 'Auth\CarAdmin\MailController@authentication')->name('caradmin.sendmail');
    Route::get('/email-resend', 'Auth\CarAdmin\MailController@emailresend')->name('caradmin.emailresend');
    Route::get('verifier/{verifier}/{email}', 'Auth\CarAdmin\CarAdminController@verifier')->name('caradmin.emailverify');
    Route::get('/emailverified', 'HomeController@verifiedpage')->name('caradmin.verified.page');
    Route::get('/email-not-verified', 'HomeController@notverifiedpage')->name('caradmin.notverified.page');
});
