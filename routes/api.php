<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

Route::post('login', 'API\UserController@login');
Route::post('register', 'API\UserController@register');
Route::post('search', 'HomeController@searchMethod')->name('addsearch');




//customer


Route::post('customer/register', 'API\Customer\AuthCustomer@RegisterCustomer');
Route::get('customer/profile', 'API\Customer\CustomerDetails@ProfileCustomer');
Route::post('customer/send-otp', 'API\Customer\CustomerDetails@OTP');
Route::post('customer/verify-otp', 'API\Customer\CustomerDetails@otpVerify');




//customer






Route::put('updatelocation/{carLocation}', 'HomeController@updatelocation')->name('updatelocation');
Route::get('getCars', 'HomeController@getCars')->name('getcars');
Route::post('addcar', 'HomeController@addcar')->name('addcar');
Route::post('addlocation', 'HomeController@addlocation')->name('addlocation');
Route::get('showlocations', 'HomeController@showLocation')->name('showLocation');
//Route::get('getNearestCars/{Customer}', 'HomeController@getNearestCars')->name('getNearestCars');
Route::get('getCityCars/{CreateCity}', 'HomeController@getCityCars')->name('getCityCars');
Route::post('addtripdriver', 'TripDriverController@addTripDriver')->name('addTrip');
Route::get('bookingconfirm/{Car}', 'TripDriverController@bookingConfirm')->name('bookingconfirm');


Route::get('home', 'HomeController@index')->name('home');

Route::prefix('admin')->group(function () {
    Route::get('profile', function () {
        return "This is a Verified Only Page";
    });
    Route::get('/', 'API\Auth\Admin\CarAdminController@index')->name('admin.dashboard');
    Route::get('dashboard', 'API\Auth\Admin\CarAdminController@index')->name('admin.dashboard');
    Route::get('register', 'API\Auth\Admin\CarAdminController@create')->name('admin.register');
    Route::post('register', 'API\Auth\Admin\CarAdminController@store')->name('admin.register.store');
    Route::get('login', 'API\Auth\Admin\LoginController@login')->name('admin.auth.login');
    Route::post('login', 'API\Auth\Admin\LoginController@loginAdmin')->name('admin.auth.loginAdmin');
    Route::post('logout', 'API\Auth\Admin\LoginController@logout')->name('admin.auth.logout');
    Route::post('sendotp', 'API\Auth\Admin\CarAdminController@sendSMS')->name('admin.sendotp');
    Route::get('otpform', 'API\Auth\Admin\CarAdminController@otpform')->name('admin.otpform');
    Route::post('otpverify', 'API\Auth\Admin\CarAdminController@otp')->name('admin.otpverify');
    Route::post('email-verify', 'API\Auth\Admin\MailController@authentication')->name('admin.sendmail');
    Route::get('email-resend', 'API\Auth\Admin\MailController@emailresend')->name('admin.emailresend');
    Route::get('verifier/{verifier}/{email}', 'API\Auth\Admin\CarAdminController@verifier')->name('admin.emailverify');
    Route::get('emailverified', 'HomeController@verifiedpage')->name('admin.verified.page');
    Route::get('email-not-verified', 'HomeController@notverifiedpage')->name('admin.notverified.page');
});

Route::prefix('customer')->group(function () {
    Route::post('otpregister', 'TempController@Store')->name('customer.otpregister');
    Route::post('otpverifyapi', 'TempController@otpverifyapi')->name('customer.otpverify');
    /*Route::get('/', 'API\Auth\Customer\CustomerController@index')->name('customer.dashboard');
    Route::get('dashboard', 'API\Auth\Customer\CustomerController@index')->name('customer.dashboard');
    Route::get('register', 'API\Auth\Customer\CustomerController@create')->name('customer.register');
    Route::post('register', 'API\Auth\Customer\CustomerController@store')->name('customer.register.store');
    Route::get('login', 'API\Auth\Customer\LoginController@login')->name('customer.auth.login');
    Route::post('login', 'API\Auth\Customer\LoginController@loginCustomer')->name('customer.auth.loginCustomer');
    Route::post('logout', 'API\Auth\Customer\LoginController@logout')->name('customer.auth.logout');
    Route::post('sendotp', 'API\Auth\Customer\CustomerController@sendSMS')->name('customer.sendotp');
    Route::get('otpform', 'API\Auth\Customer\CustomerController@otpform')->name('customer.otpform');
    Route::post('otpverify', 'API\Auth\Customer\CustomerController@otp')->name('customer.otpverify');
    Route::post('email-verify', 'API\Auth\Customer\MailController@authentication')->name('customer.sendmail');
    Route::get('email-resend', 'API\Auth\Customer\MailController@emailresend')->name('customer.emailresend');
    Route::get('verifier/{verifier}/{email}', 'API\Auth\Customer\CustomerController@verifier')->name('customer.emailverify');
    Route::get('emailverified', 'HomeController@verifiedpage')->name('customer.verified.page');
    Route::get('email-not-verified', 'HomeController@notverifiedpage')->name('customer.notverified.page');*/
});
Route::prefix('driver')->group(function () {
    Route::get('/', 'API\Auth\Driver\DriverController@index')->name('driver.dashboard');
    Route::get('dashboard', 'API\Auth\Driver\DriverController@index')->name('driver.dashboard');
    Route::get('register', 'API\Auth\Driver\DriverController@create')->name('driver.register');
    Route::post('register', 'API\Auth\Driver\DriverController@store')->name('driver.register.store');
    Route::get('login', 'API\Auth\Driver\LoginController@login')->name('driver.auth.login');
    Route::post('login', 'API\Auth\Driver\LoginController@loginDriver')->name('driver.auth.loginDriver');
    Route::post('logout', 'API\Auth\Driver\LoginController@logout')->name('driver.auth.logout');
    Route::post('/sendotp', 'API\Auth\Driver\DriverController@sendSMS')->name('driver.sendotp');
    Route::get('/otpform', 'API\Auth\Driver\DriverController@otpform')->name('driver.otpform');
    Route::post('/otpverify', 'API\Auth\Driver\DriverController@otp')->name('driver.otpverify');
    Route::post('/email-verify', 'API\Auth\Driver\MailController@authentication')->name('driver.sendmail');
    Route::get('/email-resend', 'API\Auth\Driver\MailController@emailresend')->name('driver.emailresend');
    Route::get('verifier/{verifier}/{email}', 'API\Auth\Driver\DriverController@verifier')->name('driver.emailverify');
    Route::get('/emailverified', 'HomeController@verifiedpage')->name('driver.verified.page');
    Route::get('/email-not-verified', 'HomeController@notverifiedpage')->name('driver.notverified.page');
});
Route::prefix('caradmin')->group(function () {
    Route::get('/', 'API\Auth\CarAdmin\CarAdminController@index')->name('caradmin.dashboard');
    Route::get('dashboard', 'API\Auth\CarAdmin\CarAdminController@index')->name('caradmin.dashboard');
    Route::get('register', 'API\Auth\CarAdmin\CarAdminController@create')->name('caradmin.register');
    Route::post('register', 'API\Auth\CarAdmin\CarAdminController@store')->name('caradmin.register.store');
    Route::get('login', 'API\Auth\CarAdmin\LoginController@login')->name('caradmin.auth.login');
    Route::post('login', 'API\Auth\CarAdmin\LoginController@loginCarAdmin')->name('caradmin.auth.loginCarAdmin');
    Route::post('logout', 'API\Auth\CarAdmin\LoginController@logout')->name('caradmin.auth.logout');
    Route::post('/sendotp', 'API\Auth\CarAdmin\CarAdminController@sendSMS')->name('caradmin.sendotp');
    Route::get('/otpform', 'API\Auth\CarAdmin\CarAdminController@otpform')->name('caradmin.otpform');
    Route::post('/otpverify', 'API\Auth\CarAdmin\CarAdminController@otp')->name('caradmin.otpverify');
    Route::post('/email-verify', 'API\Auth\CarAdmin\MailController@authentication')->name('caradmin.sendmail');
    Route::get('/email-resend', 'API\Auth\CarAdmin\MailController@emailresend')->name('caradmin.emailresend');
    Route::get('verifier/{verifier}/{email}', 'API\Auth\CarAdmin\CarAdminController@verifier')->name('caradmin.emailverify');
    Route::get('/emailverified', 'HomeController@verifiedpage')->name('caradmin.verified.page');
    Route::get('/email-not-verified', 'HomeController@notverifiedpage')->name('caradmin.notverified.page');
});
