<?php return array (
  'beyondcode/laravel-dump-server' => 
  array (
    'providers' => 
    array (
      0 => 'BeyondCode\\DumpServer\\DumpServerServiceProvider',
    ),
  ),
  'fideloper/proxy' => 
  array (
    'providers' => 
    array (
      0 => 'Fideloper\\Proxy\\TrustedProxyServiceProvider',
    ),
  ),
  'laravel/passport' => 
  array (
    'providers' => 
    array (
      0 => 'Laravel\\Passport\\PassportServiceProvider',
    ),
  ),
  'laravel/tinker' => 
  array (
    'providers' => 
    array (
      0 => 'Laravel\\Tinker\\TinkerServiceProvider',
    ),
  ),
  'nesbot/carbon' => 
  array (
    'providers' => 
    array (
      0 => 'Carbon\\Laravel\\ServiceProvider',
    ),
  ),
  'nunomaduro/collision' => 
  array (
    'providers' => 
    array (
      0 => 'NunoMaduro\\Collision\\Adapters\\Laravel\\CollisionServiceProvider',
    ),
  ),
  'smartins/passport-multiauth' => 
  array (
    'providers' => 
    array (
      0 => 'SMartins\\PassportMultiauth\\Providers\\MultiauthServiceProvider',
    ),
  ),
  'webpatser/laravel-uuid' => 
  array (
    'providers' => 
    array (
      0 => 'Webpatser\\Uuid\\UuidServiceProvider',
    ),
    'aliases' => 
    array (
      'Uuid' => 'Webpatser\\Uuid\\Uuid',
    ),
  ),
);